#!/bin/bash

nWorkers=$1;
if [ -z "$nWorkers" ]; then
    echo "Missing argument: number of parallel workers per matlab job."
    exit 1
fi
for nfevalExp in $(seq 1 2); do
	matlab -nosplash -nodisplay -r "mainCluster(${nfevalExp},${nWorkers});exit" &
	sleep 5
done
wait
