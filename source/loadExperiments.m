function [ experiments ] = loadExperiments( settingsOrFilename )
%LOADEXPERIMENTS ... if not loaded before.
%
%   `IQMexperiment` function wrapper.
%
    %
    if isstrcellstr(settingsOrFilename)
        settings.expFilename = settingsOrFilename;
    elseif ~isstruct(settingsOrFilename)
        error('LOADEXPERIMENTS:IllegalArgument', 'Expecting experiment file name, or cell array of file names, or settings structure.');
    else
        settings = settingsOrFilename;
    end
    if ~isfield(settings, 'experiments') || isempty(settings.experiments)
        if ~isfield(settings, 'expFilename') || isempty(settings.expFilename)
            error('LOADEXPERIMENTS:MissingExperiment', 'Please specify `expFilename` string or cell array of strings.');
        else
            fnCa = idOrCell(settings.expFilename);
            experiments = cell(size(fnCa));
            for i=1:numel(fnCa)
                experiments{i} = IQMexperiment(fnCa{i});
            end
        end
    else
        experiments = idOrCell(settings.experiments);
        for i=1:numel(experiments)
            if ~isIQMexperiment(experiments{i})
                error('LOADEXPERIMENTS:InvalidExperiment', 'The `experiments` field must be a valid `IQMexperiment`, or a cell array of such.');
            end
        end
    end


end
