function [ paramSpecs ] = loadParamSpecs( settingsOrFilename )
%LOAPARAMSPECS ... Read and process parameters which can be modified, and in
%   which range.
%
%   `readtable` wrapper
%
    %
    if ischar(settingsOrFilename)
        settings.paramSpecsFilename = settingsOrFilename;
    elseif ~isstruct(settingsOrFilename)
        error('LOAPARAMSPECS:IllegalArgument', 'Expecting parameters specification file name or settings structure.');
    else
        settings = settingsOrFilename;
    end

    if ~isfield(settings, 'paramSpecs') || isempty(settings.paramSpecs)

        if ~isfield(settings, 'paramSpecsFilename') || isempty(settings.paramSpecsFilename)
            error('LOAPARAMSPECS:MissingParamSpecs', 'Please specify `paramSpecsFilename` string.');
        else
            % table enforces fields homogenity and same columns size w/ '.COLNAME'
            % access as in struct
            paramSpecs = readtable(settings.paramSpecsFilename);
        end
    else
        paramSpecs = settings.paramSpecs;
    end

    %% Param specs table: validate and complement
    % check if columns exist with the `isfield` test for struct
    pSpecStruct = table2struct(paramSpecs);
    colnames = {'names', 'projections', 'bmin', 'bmax', 'islog'};
    for i=1:numel(colnames)
        if ~isfield(pSpecStruct, colnames{i})
            error('LOAPARAMSPECS:InvalidParamSpecs', 'Parameters specification has no `%s` column.', colnames{i});
        end
    end
    paramSpecs.islog = logical(paramSpecs.islog);
    % optional p0 column
    if ~isfield(pSpecStruct,'p0')
        paramSpecs.p0 = zeros(size(paramSpecs,1),1);
    end
    % non-zero p0 explicitly provided in settings has precedence over one in parameters specification
    if isfield(settings, 'p0') && ~all(settings.p0==0)
        if all(size(settings.p0) == size(paramSpecs.p0))
            paramSpecs.p0 = settings.p0;
        elseif all(size(settings.p0') == size(paramSpecs.p0))
            paramSpecs.p0 = settings.p0';
        else
            error('LOAPARAMSPECS:InvalidSettingsP0', 'Expecting initial parameter values vector of size %d.', size(paramSpec.p0,1));
        end
    end

end
