function [viablePoints, cost, sampled] = prepareViablePointsForProjection(...
    currentProjection, witnessPoint, viablePoints, threshold,...
    options, paramSpecs, evalModelOPTIONS...
)
    % Inform evalModel
    fixedParamsMask = evalModelOPTIONS.fixSpecParamIdxs(currentProjection);

    % Filter viable points for the current projection
    localModifiableParameterIdxs = find(~currentProjection);
    % the actual projection of points (in case it hasn't been done before)
    viablePoints = setRmCols(viablePoints, localModifiableParameterIdxs);

    % sparsify (before evaluation) if too many points (due to merging)
    % from each found group of points, choose points closest to the max projection
    maxProjectionPoint = evalModelOPTIONS.paramFixValues(evalModelOPTIONS.unfixedSpecParamIdxs);
    maxProjectionPoint = reshape(maxProjectionPoint, 1, numel(maxProjectionPoint)); % make sure it's a row vector
    viablePoints = setSparsify(viablePoints, options.nvmax, maxProjectionPoint);

    nViablePointsOld = size(viablePoints, 1);
    [viablePoints, cost] = filterViablePoints(viablePoints, threshold);
    nViablePoints = size(viablePoints, 1);
    if nViablePoints == 0
        %warning('PREPAREVIABLEPOINTSFORPROJECTION:NoViablePoints', 'No viable points left.\n');
        %return
        % witness outed in the pre-filtering sparsification => re-add
        viablePoints = setRmCols(witnessPoint, localModifiableParameterIdxs);
        nViablePoints = size(viablePoints, 1); % == 1
    end
    fprintf('[INFO] prepareViablePointsForProjection, %d out of %d (unique and sparsfied) viable points left for projection: %s.\n', nViablePoints, nViablePointsOld, mat2str(int8(currentProjection)));
    if options.saveViablePoints
        saveViablePoints('projected', currentProjection, viablePoints, cost,...
                paramSpecs, options.outputFilename);
    end


    % Get new viable points if necessary
    [viablePoints, cost, sampled] = resampleViablePoints(localModifiableParameterIdxs, viablePoints, cost, threshold, options, paramSpecs);
    if sampled
        % sparsify (after sampling)
        [viablePoints, Isparse] = setSparsify(viablePoints, options.nvmax, maxProjectionPoint);
        cost = cost(Isparse);
        fprintf('[INFO] prepareViablePointsForProjection, number of viable points after sampling: %d.\n', size(viablePoints, 1));
        if options.saveViablePoints
            saveViablePoints('sampled', currentProjection, viablePoints, cost,...
                paramSpecs, options.outputFilename);
        end
    end

    % extend viablePoints back to the root model size
    nViablePoints = size(viablePoints,1);
    extendedViablePoints = zeros(nViablePoints,numel(currentProjection));
    paramsDefaultValues = transpose(paramSpecs.projections); % this must be a row vector to extend paramSampleI to a row vector
    for i = 1:nViablePoints
        paramSampleI = viablePoints(i,:);
        extendedViablePoints(i,:) = extendVec(currentProjection, paramsDefaultValues, paramSampleI);
    end
    viablePoints = extendedViablePoints;
    % Reset evalModel
    evalModelOPTIONS.varySpecParamIdxs(fixedParamsMask);
end



function [viablePoints, cost] = filterViablePoints(points, threshold)
    n = size(points, 1);
    isNewViablePoint = false(n, 1);
    cost = nan(n,1);
    for i = 1:n
        newParameterPoint = points(i,:);
        cost(i) = evalModel(newParameterPoint);
        if (cost(i) < threshold)
            isNewViablePoint(i) = true;
        end
    end
    viablePoints = points(isNewViablePoint,:);
    cost = cost(isNewViablePoint);
end
