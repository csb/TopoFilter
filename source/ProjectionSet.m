classdef ProjectionSet < handle
%PROJECTIONSSET Set of projections.
%
% Projection is given as a logical vector, where coordinates correspond to
% parameters, and value 1 denots that this parameter can be projected.
%
% Order on projections is given by a coordinate-wise order.
% For instance, both [ 1 0 1 ] and [ 1 1 0 ] projections are larger than
% projection [ 1 0 0 ], but neither of the former is larger then the other. Both
% [ 1 0 1 ] and [ 1 1 0 ] are maximal projections among the three projections.
%
% And operator on projections is a bit-wise AND; we have: if p1 <= p2,then
% p1 AND p2 == p2.
%
% Remark: AND combinations of non-maximal projections can give rise to new
%         maximal projections.
%
    methods(Static, Sealed)
        function bool = pleq(p1, p2)
            bool = all(logical(p1) <= logical(p2));
        end
        function bool = peq(p1, p2)
            bool = all(~xor(p1, p2));
        end
    end
    methods(Static)
        function [Crowmat, IArowmat, ICrowmat] = punique(Arowmat, varargin)
        %PUNIQUE Wrapper around unique, for application to rows of a matrix.
        %
            [Crowmat, IArowmat, ICrowmat] = unique(Arowmat, 'rows', varargin{:});
        end
    end
    methods(Static, Access = protected)
        function obj = p_new(varargin)
            obj = ProjectionSet(varargin{:});
        end
    end
    properties (Dependent, Access = public)
        dim
        num
        rowmat
        parrowmat
    end
    properties (Access = protected)
        p_projections = false(0);
        p_parSamples = []; % simplified: one witness sample per each projection
        p_lastIdx = 0;
    end
    methods (Access = protected)
        function p_init(self,dim,varargin)
        % initialise protected fields
        %
            if isempty(varargin)
                n = 8;
            else
                n = varargin{1};
            end
            self.p_projections = false(dim,n);
            self.p_parSamples = zeros(dim,n);
            self.p_lastIdx = 0;
        end
        function p_copy(self,other)
        % copy protected fields
        %
            self.p_projections = other.p_projections;
            self.p_parSamples = other.p_parSamples;
            self.p_lastIdx = other.p_lastIdx;
        end
        function grew = p_grow(self)
        % Double projections and samples matrices
        %
            grew = false;
            if (self.num > size(self.p_projections,2))
                self.p_projections = doubleMatrixWidth(self.p_projections, @false);
                self.p_parSamples = doubleMatrixWidth(self.p_parSamples, @zeros);
                grew = true;
            end
        end
        function p_incr(self)
        % Increment number of projections
        %
            self.p_lastIdx = self.p_lastIdx + 1;
            % grow cache space if necessary
            self.p_grow();
        end
        function p_add(self, projection, parSample)
        % Add unconditionaly
        %
            self.p_incr();
            self.p_projections(:,self.num) = projection;
            self.p_parSamples(:,self.num) = parSample;
        end
        function bool = p_hasAt(self, i, pCol)
            bool = ProjectionSet.peq(pCol, self.at(i));
        end
    end
    methods
        %%%% Dependent properties
        function d = get.dim(self)
        % DIM Dimension of projections
        %
            d = size(self.p_projections,1);
        end
        function n = get.num(self)
        % NR Number of projections
        %
            n = self.p_lastIdx;
        end
        function projectionsRowArray = get.rowmat(self)
        % ROWMAT Return cached projections in rows of an array.
        %
            projectionsRowArray = transpose(self.p_projections(:,1:self.num));
        end
        function parSampleRowArray = get.parrowmat(self)
        % PARROWMAT Return cached parameters samples in rows of an array.
        %
            parSampleRowArray = transpose(self.p_parSamples(:,1:self.num));
        end
    end
    methods (Access = public)
        %%%% Constructors
        function self = ProjectionSet(varargin)
        % PROJECTIONSSET ...
        %
        % Inputs:
        %    dim (integer): dimension of projections (nr of parameters).
        %    n   (integer): optional, expected number of projections;
        %                   default: 64.
        % or
        %    other (ProjectionSet): a projections set to clone
        %
            if nargin < 1 % cf. "Initialize Object Arrays"
                return
            end
            dim = varargin{1};
            self.p_init(dim,varargin{2:end});
        end
        % OPT to make this class code clean (no knowledge of
        %     MaxProjectionSet here) use cell array in addAll
        function maxSelf = MaxProjectionSet(self)
        %MAXPROJECTIONSET converter method for subclass to seemlessly use
        % addAll@MaxProjectionSet with an array of ProjectionSet objects.
        %
            maxSelf = MaxProjectionSet();
            maxSelf.p_copy(self);
        end
        %%%% Methods
        function answer = isempty(self)
            answer = (self.num == 0);
        end
        function [projection, parSample] = at(self, i)
        % AT Return i-th projection and associated parameter sample.
        %
            if any(i > self.num)
                error('PROJECTIONSSET:AT', 'Index exceeds number of projections.');
            end
            projection = self.p_projections(:,i);
            if nargout > 1
                parSample = self.p_parSamples(:,i);
            end
        end
        function reduced = reduceTo(self, idxs)
        % REDUCETO Return a copy of projections reduced to given indices.
        %
            reduced = self.p_new();
            [reduced.p_projections, reduced.p_parSamples] = self.at(idxs);
            reduced.p_lastIdx = size(reduced.p_projections,2);
        end
        function copied = copy(self)
        % COPY Return a copy.
        %
            copied = self.reduceTo(1:self.num);
        end
        function [answer, idx] = has(self, projection)
        % HAS Check if projection is already in the set. Returns logic answer
        % and also index in the set if true, otherwise 0.
        %
            answer = false;
            idx = 0;
            if  self.dim == 0
                return
            elseif numel(projection) ~= self.dim
                error('PROJECTIONSSET:HAS', 'Incompatible projection dimension.');
            end
            pCol = projection(:);
            % TODO would that have better performance?
            %answer = ismember(transpose(pCol),self.rowmat,'rows');
            for i = 1:self.num
                if self.p_hasAt(i, pCol)
                    answer = true;
                    idx = i;
                    break;
                end
            end
        end
        function added = add(self, projection, varargin)
        % ADD Add a projection if not already in the set. Optionally add
        %  also an assosciated parameter sample.
        %
            if isempty(varargin)
                parSample = nan(size(projection));
            else
                parSample = varargin{1};
            end

            if self.dim == 0
                self.p_init(numel(projection))
            elseif numel(projection) ~= self.dim
                error('PROJECTIONSSET:ADD', 'Incompatible projection dimension.');
            end
            if numel(parSample) ~= self.dim
                error('PROJECTIONSSET:ADD', 'Incompatible parameter sample dimension.');
            end

            added = false;
            % TODO save also errors and even if the projection is already saved,
            %      update the parameter sample if it has a lower error
            if ~self.has(projection)
                self.p_add(projection, parSample);
                added = true;
            end
        end
        function [IPadded, IPpresent, IIPaddedDuplicates, IselfPresent] = addAll(self, P)
        % ADDALL Add all new unique projections from another projection set
        %  or array of such.
        %
        % Inputs:
        %     P: projection set or struct array of projection sets.
        %
        % Outputs:
        %     IPadded:
        %         a column vector of indices of projections from P, which
        %         were actually added to the set. In case of an array
        %         input, the indices are computed with respect to P
        %         cumulatively, i.e. as if the array would be a flat list
        %         (not set) of projections.
        %     IPpresent:
        %         a column vector of indices of projections from P, which
        %         were previously present in the set. In case of an array
        %         input indices are cumulatve, analogously as for IPadded.
        %     IIPaddedDuplicates:
        %         a vector of indices of IPadded which identifies
        %         duplicates among actually added projections. If the input
        %         is a projection set, then this vector is simply
        %         1:numel(IPadded). Otherwise, i.e. when P is an array of
        %         projection sets, duplicates of added projection may
        %         happen.
        %    IselfPresent:
        %         a vector of indices of projections in the set (pre and post
        %         addition), which were already present, i.e. for which there
        %         was at least one projection not added to the set
        %         (cf. Ipresent)
        %
        % Example:
        %
        %    projSet = ...
        %    projectionSetArray = ...
        %
        %    projSetBefore = projSet.copy();
        %    [Iadded, Ipresent, IIaddedDups] = projSet.addAll(projectionSetArray);
        %
        %    % flatten projections array
        %    projRowmatCa = arrayfun(@(ps) ps.rowmat, projectionSetArray, 'UniformOutput', false);
        %    projRowmat = vertcat(projRowmatCa{:});
        %
        %    % new set of projections is the old ones plus the actually
        %    % added ones, in that order
        %    projAddedRowmat = projRowmat(Iadded,:);
        %    projSet.rowmat == [projSetBefore.rowmat; projAddedRowmat]
        %
        %    % in the projections array there were projections already
        %    % present in the set plus the actually added ones and their
        %    % duplicates
        %    size(projRowmat,1) == numel(Ipresent) + numel(IIaddedDups)
        %    projAddedWithDuplicatesRowmat = projRowmat(Iadded(IIaddedDups),:);
        %    projNotAdded = projRowmat(Ipresent,:);
        %
            if ~isa(P,'ProjectionSet')
                error('PROJECTIONSSET:ADDALL', 'Expected ProjectionSet instance or array of such.');
            end

            % prep single row array of projections and parameters samples
            PA = [self P];
            nPA = numel(PA);
            projTCa = cell(nPA,1);
            parSampTCa = cell(nPA,1);
            for i = 1:nPA
                projTCa{i} = PA(i).rowmat;
                parSampTCa{i} = PA(i).parrowmat;
            end
            try
                projTUnion = cell2mat(projTCa);
                parSampTUnion = cell2mat(parSampTCa);
            catch ME
                if (strcmp(ME.identifier,'MATLAB:catenate:dimensionMismatch'))
                    error('PROJECTIONSSET:ADDALL', 'Incompatible projections dimensions.');
                else
                    rethrow(ME);
                end
            end

            % get unique rows; important: 'stable' to avoid re-ordering
            % of the original projections
            [projTUnique, IprojTUnionToUnique, IprojTUniqueToUnion] = ...
                self.punique(projTUnion,'stable');
            % number of projections in the initial set
            num0 = self.num;
            % number of unique projections in the initial set
            num1 = find(IprojTUnionToUnique <= num0, 1, 'last');
            % Note: because 'stable' was used, num0 == num1 in case of
            %       ProjectionSet, but num0 >= num1 in case of
            %       MaxProjectionSet
            if isempty(num1), num1 = 0; end
%             % TEMP
%             if numel(IprojTUnionToUnique) > 0 && num1 > 0
%                 Iproj0ToUnique = IprojTUnionToUnique(1:num0);
%                 assert(issorted(Iproj0ToUnique), 'Non-stable unique.');
%                 assert(all(ismembc(Iproj0ToUnique, transpose(1:num0))),...
%                     'Further projection duplicates picked over the ones in the current set.');
%             end

            % keep all the original projections, add all new unique ones
            % Again, since 'stable' used, this only matters for
            % MaxProjectionSet
            projT0 = self.rowmat;
            parSampT0 = self.parrowmat;
            projections = [projT0; projTUnique(num1+1:end,:)]';
            lastIdx = size(projections,2);
            % OPT when tracking errors, for duplicate projections select
            %     the lowest error parameters sample
            parSampTUnique = parSampTUnion(IprojTUnionToUnique,:);
            parSamples = [parSampT0; parSampTUnique(num1+1:end,:)]';
            self.p_projections = projections;
            self.p_lastIdx = lastIdx;
            self.p_parSamples = parSamples;

            % compute added indices, knowing that puniqe was stable, so
            % first num0 rows of the projTUnion are projections in the set
            % prior to the addition and they map to first num1 rows of
            % projTUnique
%             disp('#### TEMP')
%             disp('projTUnion')
%             disp(projTUnion)
%             disp('IprojTUnionToUnique')
%             disp(IprojTUnionToUnique)
%             disp('projTUnique')
%             disp(projTUnique)
%             disp('IprojTUniqueToUnion');
%             disp(IprojTUniqueToUnion);
            IPadded = IprojTUnionToUnique(IprojTUnionToUnique > num0) - num0;
            IPUniqueToUnion = IprojTUniqueToUnion((num0+1):end);
            IPpresentMask = (IPUniqueToUnion <= num1);
            IPpresent = find(IPpresentMask);
            IIPaddedDuplicates = IPUniqueToUnion(IPUniqueToUnion > num1) - num1;
            IselfUniqueToUnion = IprojTUniqueToUnion(1:num0);
            IselfPresent = unique(IselfUniqueToUnion(IPUniqueToUnion(IPpresentMask)));
            % TEMP
            assert(numel(IPadded) == (self.num - num0)...
                && (numel(IIPaddedDuplicates)+numel(IPpresent)) == (size(projTUnion,1) - num0),...
                '\tnew size = %d\n\tinit size = %d\n\t#to add = %d\n\t#unique in initial = %d\n\t#added = %d\n\t#added (w/ duplicates) = %d\n\t#already present = %d', self.num, num0, size(projTUnion,1) - num0, num1, numel(IPadded), numel(IIPaddedDuplicates), numel(IPpresent));
        end

    end
end

function Awide = doubleMatrixWidth(A, initHandle)
    Awide = [A initHandle(size(A))];
end
