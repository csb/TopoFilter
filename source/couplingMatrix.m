function C = couplingMatrix(d, parameterIdxs)
%COUPLINGMATRIX Return logical d x d matrix C which specifies coupling of
% projections of parameters in the following form:
%    C(i,j) = if param i is projected then project also param j
%
% This is a default implemenation, with no copuling. Speficy yours via
% copulingMatrixFcn option of the experiment.
%
% Input vector parameterIdxs is a set of currently projectable parameter indices
% (rows in parameter specification). It allows for dynamic definitions (see
% example below). It is a subset of all modifiable parameter indices 1:d. Its
% complement to the vector 1:d is a set of either nonprojectable or already
% projected indices.
%
% Example 1: Symmetric coupling
% Always project together params in rows 1 and 2 in params specification:
%     C(1,2) = true;
%     C(2,1) = true;
%
% Example 2: Assymetric coupling
% When param in row 2 in params specification (think: v_max) is projected
% then project also param in row 1 in params specification (think: Km),
% but not the other way around:
%     C(2,1) = true;
%
% Example 3: Dynamic coupling
% If 1 (Vmax) is projected than also project 2, 3, 4 (Km1, Km2, Km3);
% if all Kmi are projected (think: to infinity, s.t. Xi/Kmi = 0 in the
% regulatory part of reaction rate), then project also Vmax.
%     C(1,2) = true;
%     C(1,3) = true;
%     C(1,4) = true;
%     C(4,1) = ~any(ismember([2 3],parameterIdxs));
%     C(3,1) = ~any(ismember([2 4],parameterIdxs));
%     C(2,1) = ~any(ismember([3 4],parameterIdxs));
%

    % initialise: no coupling
    C = diag(true(d,1));

end
