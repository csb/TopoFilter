function saveViablePoints(typeName, projection, viablePointsRowmat, cost, paramSpecs, outputFilename)
    [pathstr,fName,~] = fileparts(outputFilename);
    viablePointsName = sprintf('viablePoints_%s_%d_%s',typeName,clockStamp(),logicalbitstr(projection));
    outputFilename = fullfile(pathstr, sprintf('%s_%s.mat',fName,viablePointsName));

    viablePoints.rowmat = viablePointsRowmat;
    viablePoints.cost = cost;
    viablePoints.colnames = paramSpecs.names(~projection);
    viablePoints.islog = paramSpecs.islog(~projection);
    viablePoints.projection = projection;
    viablePoints.projected.names = paramSpecs.names(projection);
    viablePoints.projected.values = paramSpecs.projections(projection);
    viablePoints.projected.islog = paramSpecs.islog(projection);

    save(outputFilename, 'viablePoints');
end

function str = logicalbitstr(l)
    str = strrep(mat2str(int8(l(:))),';','');
    str = str(2:(end-1));
end
