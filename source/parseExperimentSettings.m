function [ settings ] = parseExperimentSettings( varargin )
%PARSEEXPERIMENTSETTINGS ...
%
%   Sets default values if not provided.
%
%   See `templateExperiment.m` for description of avaialable parameters.
%

    % Set default values for parameters that are not provided
    parser = inputParser;
    parser.FunctionName = 'PARSEEXPERIMENTSETTINGS: ';

    addParameter(parser, 'outputFilename', sprintf('%s.mat',tempname), @ischar);

    addParameter(parser, 'numRuns', 1, @isposint);
    addParameter(parser, 'dryRun', false, @islogical);

    addParameter(parser, 'nmont',  89, @isposint); % 4/9 of nmont+nelip
    addParameter(parser, 'nelip', 111, @isposint); % 5/9 of nmont+nelip

    addParameter(parser, 'nvmin',  20, @isposint); % 1/10 of nmont+nelip
    addParameter(parser, 'nvmax', 380, @isposint); % 2*(nmont+nelip)-nvmin

    addParameter(parser, 'saveViablePoints', false, @islogical);

    addParameter(parser, 'dropMontPoints', false, @islogical);

    addParameter(parser, 'recursive', true, @islogical);
    addParameter(parser, 'enumLevel', 1, isinrange(2));
    addParameter(parser, 'exhaustiveRank', 1, @isposint);
    addParameter(parser, 'backtrack', false, @islogical);
    addParameter(parser, 'pointsAsOuter', true, @islogical);
    %addParameter(parser, 'useOATheuristics', false, @islogical);

    addParameter(parser, 'parallelize', 0, isinrange(3));

    addParameter(parser, 'model', []);
    addParameter(parser, 'modelFilename', '', @ischar);

    addParameter(parser, 'experiments', []);
    addParameter(parser, 'expFilename', {}, @isstrcellstr);

    addParameter(parser, 'expData', []);
    addParameter(parser, 'expDataFilename', {}, @isstrcellstr);
    addParameter(parser, 'expDataIdxs', [], @(x) all(isposint(x)));

    addParameter(parser, 'paramSpecs', []);
    addParameter(parser, 'paramSpecsFilename', '', @ischar);
    addParameter(parser, 'couplingMatrixFcn', @couplingMatrix, @isf);
    addParameter(parser, 'p0', 0, @isnumeric);

    addParameter(parser, 'calcCostFcn', @calcCost, @isf);
    addParameter(parser, 'calcCostFcnArgs', {}, @iscell);
    addParameter(parser, 'calcThresholdFcn', @calcThresholdQuantile, @isf);
    addParameter(parser, 'calcThresholdFcnArgs', {}, @iscell);

    addParameter(parser, 'odeIntegratorOptions', struct(), @isstruct);
    addParameter(parser, 'odeIntegratorMaxnumstepsTry', 3, @isposint);
    addParameter(parser, 'odeIntegratorSolNrTimepoints', 0, @isint);

    % Parse the input arguments
    parser.KeepUnmatched = true;
    parse(parser, varargin{:});
    settings = parser.Results;

    %% Further options processing

    % translate enumLevel to projectionSetCls and dropOATProjections options
    if settings.enumLevel > 0
        settings.projectionSetCls = @ProjectionSet;
    else
        settings.projectionSetCls = @MaxProjectionSet;
    end
    if settings.enumLevel > 1
        settings.dropOATProjections = false;
    else
        % Rem: don't drop OAT proj. if it is also a OAT-sum proj.
        settings.dropOATProjections = true;
    end

    % flatten odeIntegratorOptions
    settings.odeIntegratorOptions.maxnumstepsTry = settings.odeIntegratorMaxnumstepsTry;
    settings.odeIntegratorOptions.solNrTimepoints = settings.odeIntegratorSolNrTimepoints;

    % compute number of workers for parallel pool
    if (settings.parallelize > 0)
        try
            ppool = gcp; % will create parallel pool if one hasn't been created yet
            settings.numWorkers = ppool.NumWorkers;
            assert(isposint(settings.numWorkers));
        catch ME
            warning('PARSEEXPERIMENTSETTINGS:MissingDistComp','Parallel Computing Toolbox not found. Switching off parallelization.\nThe follwoing error was caught while calling gcp:\n\t%s: %s\n',ME.identifier, ME.message);
            settings.parallelize = 0;
            settings.numWorkers = 0;
        end
    else
        settings.numWorkers = 0;
    end
end
