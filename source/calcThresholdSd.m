function [ threshold ] = calcThresholdSd( nY, logDetS, varargin )
%CALCVIABILITYTHRESHOLD Compute the viability threshold value using multiples
% of std deviation from the mean of the sum of squares of normally distributed
% residuals.
%
% Arguments:
%     nY     (numeric vector): size of the residuals vector
%     logDetS       (numeric): log-determinant of the residuals covariance
%                              matrix S
%     sdmul (optional, numeric): multiplier of the likelihood std deviation
%                                (default: 2)
%
% Residuals are assumed to have a multi-variate normal distribution N(0, S).
% Threshold is given on a negative log-likelihood l(Y; M, p) = -ln(f(Y | M, p)),
% and we have:
%    l(Y; M, p) = -ln(f(Y | M, p)) = const + (RSSE / 2),
% where RSSE (sum of square normally distributed residuals) has a Chi2
% distribution, and const = log((2*pi)^nY * det(S))/2 is the log-PDF constant
% for a multi-variate normal distribution PDF. The resulting threshold is then
% given as:
%        const + (E(RSSE) + StdMul * SD(RSSE)) / 2
%
% Methods is exact under assumption that covariance matrix S is diagonal.
% For nY = 20, sdmul = 1,2,3 correspond to, respectively, ca. 84.4%, 96.3%,
% 99.3% quantiles; for nY = 100, respectively, to ca. 84.2%, 97.0%, 99.6%
% quantiles.
%
% Rem: this threshold formula is equivalent to one, which has been derived via
%      differential entropy in the original paper by Sunnaker et al. (2013, Sci
%      Sig), and has been also used in the lab report by Moeller (2015, ETHZ).
%
    p = inputParser;

    addRequired(p,'nY',@isposint);
    addRequired(p,'logDetS',@isnumeric);
    addOptional(p, 'sdmul', 2, @isnumeric);
    parse(p, nY, logDetS, varargin{:});
    sdmul = p.Results.sdmul;

    offset = (nY*(log(2*pi)) + logDetS)/2;
    ev = nY;
    sd = sqrt(2*nY);
    threshold = offset + (ev + sdmul*sd)/2;
end
