function [ p0, error0 ] = getViableInitPoint( paramSpecs, viabilityThreshold )
%GETVIABLEINITPOINT ...
%
%   TODO: always try to update on the proposal, starting w/ paramSpec.p0 (if
%         provided), then model values (paramSpec.values; if within range), then
%         middle of the param space box ((bmin-bmax)/2 + bmin)
%

checked = false;
if (all(paramSpecs.p0 == 0))
    % Try the model values, or, if they don't satisfy specified bounds, the middle
    if all((paramSpecs.values <= paramSpecs.bmax) & (paramSpecs.values >= paramSpecs.bmin))
        p0prop = paramSpecs.values;
    else
        p0prop = (paramSpecs.bmax - paramSpecs.bmin)/2 + paramSpecs.bmin;
    end
    error0 = evalModel(p0prop);
    if error0 <= viabilityThreshold
        p0 = p0prop;
        checked = true;
    else
        %% Get initial point for parameter exploration w/ simplexIQM optimization
        simplexOPTIONS.maxfunevals = 1000;
        simplexOPTIONS.maxiter = 1000;
        simplexOPTIONS.maxtime = 120;
        simplexOPTIONS.tolx = 1e-10;
        simplexOPTIONS.tolfun = 1e-10;
        simplexOPTIONS.lowbounds = paramSpecs.bmin;
        simplexOPTIONS.highbounds = paramSpecs.bmax;
        simplexOPTIONS.outputFunction = '';
        simplexOPTIONS.silent = 0;
        [p0,error0,exitflag] = simplexIQM('evalModel',p0prop,simplexOPTIONS);
        p0 = reshape(p0,size(p0prop));
        %if exitflag == 0
        %    error('GETVIABLEINITPOINT:SimplexFail', 'simplexIQM did not find optimum and returned exit flag 0.');
        %end
        errStrPref = 'Found';
    end
else
    p0 = paramSpecs.p0;
    error0 = evalModel(p0);
    errStrPref = 'Provided';
end

if ~checked
    %% Check viability threshold
    if error0 > viabilityThreshold
        error('GETINITVIABLEPOINT:NotViable', '%s starting point is not viable; error: %.3d > expData.threshold: %.3d.',errStrPref,error0,viabilityThreshold);
    end

    %% Check p0 bounds; consistently with HYEPERSPACE: non-strict (cf. OEAMC)
    ltmin = find(p0 < paramSpecs.bmin, 1);
    gtmax = find(p0 > paramSpecs.bmax, 1);
    if (~isempty(ltmin) || ~isempty(gtmax))
        error('GETINITVIABLEPOINT:OutOfBounds', '%s starting point does not respect boundaries.',errStrPref);
    end
end

end
