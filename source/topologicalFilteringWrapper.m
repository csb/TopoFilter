function [ essentialParameters, projections, paramSamples, p0, error0 ] = ...
    topologicalFilteringWrapper( ...
        paramSpecs, threshold, dryRun, topoFilterOPTIONS, evalModelOPTIONS )
%TOPOLOGICALFILTERINGWRAPPER Do the topological filtering, and prepare results
%
debugging = (debuglevel > 0);

    % Transform parameters to log space where required
    paramSpecs = logParamSpecs(paramSpecs);

    % Reset evalModel options due to paramSpecs change
    evalModelOPTIONS = EvalModelOptions(evalModelOPTIONS.mexNames, ...
        evalModelOPTIONS.expData, paramSpecs, evalModelOPTIONS.odeIntegratorOptions);

    % Get a starting point for the parameter space exploration
    [p0, error0] = getViableInitPoint(paramSpecs, threshold);

    if debugging
        fprintf('[DEBUG] topologicalFilteringWrapper, p0 = %s (log-space = %s).\n', mat2str(p0), mat2str(int8(paramSpecs.islog)));
    end

    viablePoints = transpose(p0(:));

    % Perform topological filtering
    if dryRun
        projections = false(0,numel(p0));
        paramSamples = zeros(0,numel(p0));
    else
        viableProjections = topologicalFiltering(threshold, viablePoints, ...
            topoFilterOPTIONS, paramSpecs, evalModelOPTIONS);
        projections = viableProjections.rowmat;
        paramSamples = viableProjections.parrowmat;
    end

    % Transform parameter values back from log space
    paramSamples(:,evalModelOPTIONS.inLogSpace) = 10.^paramSamples(:,evalModelOPTIONS.inLogSpace);
    p0(evalModelOPTIONS.inLogSpace) = 10.^p0(evalModelOPTIONS.inLogSpace);

    % Change parameter values to projection values
    for i = 1:size(projections,1)
        paramSamples(i,projections(i,:)) = evalModelOPTIONS.paramFixValues(projections(i,:));
    end

    % Extract essential parameters
    essentialParameterIdxs = (any(projections,1)==0);
    if (any(essentialParameterIdxs)) % if there are any essential parameters
        essentialParameters = paramSpecs.names(essentialParameterIdxs);
    else
        essentialParameters = [];
    end

end

% Move to an ParamteresSpecification object if such will be created.
function paramSpecs = logParamSpecs(paramSpecs)
    paramSpecs.bmin(paramSpecs.islog) = log10(paramSpecs.bmin(paramSpecs.islog));
    paramSpecs.bmax(paramSpecs.islog) = log10(paramSpecs.bmax(paramSpecs.islog));
    paramSpecs.p0(paramSpecs.islog) = nonZeroLog10(paramSpecs.p0(paramSpecs.islog));
    paramSpecs.values(paramSpecs.islog) = nonZeroLog10(paramSpecs.values(paramSpecs.islog));
    paramSpecs.projections(paramSpecs.islog) = nonZeroLog10(paramSpecs.projections(paramSpecs.islog));
end

function logv = nonZeroLog10(v)
    logv = v;
    nonZeroMask = (v~=0);
    logv(nonZeroMask) = log10(v(nonZeroMask)); % Note: works fine with NaN
end
