function [ expData ] = loadExpData( settingsOrFilename, varargin )
%LOADEXPDATA ... Read and process the experimental data, if not done before.
%
%   Wrapper for ExpData(...)
%
    %
    if ischar(settingsOrFilename)
        settings.expDataFilename = settingsOrFilename;
    elseif ~isstruct(settingsOrFilename)
        error('LOADEXPDATA:IllegalArgument', 'Expecting experimental data file name, or cell array of file names, or settings structure.');
    else
        settings = settingsOrFilename;
    end
    if nargin > 1
        settings.expDataIdxs = varargin{1};
    end
    if ~isfield(settings, 'expData') || isempty(settings.expData)
        if ~isfield(settings, 'expDataFilename') || isempty(settings.expDataFilename)
            error('LOADDATA:MissingExpData', 'Please specify `expDataFilename` string or cell array of strings.');
        else
            if isfield(settings, 'expDataIdxs')
                if ~all(isposint(settings.expDataIdxs))
                    error('LOADDATA:IllegalArgument', 'Expecting `expDataIdxs` to be a vector of indices (positive integers).');
                end
            else
                settings.expDataIdxs = [];
            end
            if isfield(settings, 'calcCostFcn')
                if ~isf(settings.calcCostFcn)
                    error('LOADDATA:IllegalArgument', 'Expecting `calcCostFcn` to be a cost function name or handle.');
                end
            else
                settings.calcCostFcn = @calcCost;
            end
            if isfield(settings, 'calcCostFcnArgs')
                if ~iscell(settings.calcCostFcnArgs)
                    error('LOADDATA:IllegalArgument', 'Expecting `calcCostFcnArgs` to be a cell array of extra arguments for the cost function.');
                end
            else
                settings.calcCostFcnArgs = {}; % indicates default cost function
            end
            if isfield(settings, 'calcThresholdFcn')
                if ~isf(settings.calcThresholdFcn)
                    error('LOADDATA:IllegalArgument', 'Expecting `calcThresholdFcn` to be a cost threshold function name or handle.');
                end
            else
                settings.calcThresholdFcn = @calcThresholdQuantile;
            end
            if isfield(settings, 'calcThresholdFcnArgs')
                if ~iscell(settings.calcThresholdFcnArgs)
                    error('LOADDATA:IllegalArgument', 'Expecting `calcThresholdFcnArgs` to be a cell array of extra arguments for the threshold function.');
                end
            else
                settings.calcThresholdFcnArgs = {}; % indicates default threshold function
            end
            expData = ExpData(settings.expDataFilename, settings.expDataIdxs,...
                settings.calcCostFcn, settings.calcCostFcnArgs,...
                settings.calcThresholdFcn, settings.calcThresholdFcnArgs);
        end
    else
        expData = settings.expData;
    end
end
