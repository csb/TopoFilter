function [ threshold ] = calcThresholdQuantile( nY, logDetS, varargin )
%CALCTHRESHOLDQUANTILE Compute the viability threshold value using a quantile
% of the negative log-likleihood function.
%
% Arguments:
%    nY      (numeric vector): size of the residuals vector
%    logDetS        (numeric): log-determinant of the residuals covariance
%                              matrix S
%    prob (optional, numeric): probability of a (one-tailed) quantile function
%                              for dostribution of l(Y; M, p); default: 0.95.
%
% Residuals are assumed to have a multi-variate normal distribution N(0, S).
% Threshold is given on a negative log-likelihood l(Y; M, p) = -ln(f(Y | M, p)),
% and we have:
%    l(Y; M, p) = const + (RSSE / 2),
% where RSSE (sum of square normally distributed residuals) has a Chi2
% distribution, and const = log((2*pi)^nY * det(S))/2 is the log-PDF constant
% for a multi-variate normal distribution PDF.
%
% Method is exact under assumption that covariance matrix S is diagonal.
%
% Rem: because Chi2 distribution is assymetric, the (one-tailed) quantile
%      approach is recommended over the original SD multiplier method.
%
    p = inputParser;
    addRequired(p,'nY',@isposint);
    addRequired(p,'logDetS',@isnumeric);
    addOptional(p, 'prob', 0.95, @isprob);
    parse(p, nY, logDetS, varargin{:});
    prob = p.Results.prob;

    offset = (nY*(log(2*pi)) + logDetS)/2;
    threshold = offset + chi2inv(prob,nY)/2;
end
