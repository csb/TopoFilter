function cost = evalModelBacktrackTesting(parameters)
%TEMP temporary evalModel funciton for testing backtracking
%
	projection = parameters == 0;

    if     all(projection == [1 1 1 0 0 0 0])
		cost = 1;
	elseif all(projection == [1 1 0 1 0 0 0])
		cost = 1;
	elseif all(projection == [0 1 1 1 0 0 0])
		cost = 1;
	elseif all(projection == [1 0 0 1 0 0 0])
		cost = 1;
	else
		cost = 1000;
	end
end
