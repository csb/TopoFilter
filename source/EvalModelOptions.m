classdef EvalModelOptions < handle
%EVALMODELOPTIONS Options for the `evalModel` function.
%
% Properties:
%   mexNames
%   odeIntegratorOptions
%
%   expData
%
% [Dependent]
%   paramNames
%   paramDefaultValues
%   paramFixValues
%   inLogSpace
%
%   fixedSpecParamIdxs
%   impossibleToFixSpecParamIdxs
%   possibleToFixSpecParamIdxs
%   modifiableModParamIdxs
%
%
% Note: no copy interface (matlab.mixin.Copyable); instead, you keep code clean
%       and always use fix*ParamIdxs/vary*ParamIdxs pair of methods.
%       Moreover, there won't be any conflicts in parfor iterations for handle
%       objects; cf.
%       https://ch.mathworks.com/help/distcomp/objects-and-handles-in-parfor-loops.html
%
   properties (Access = public)
        mexNames % immutable
        expData % immutable
        odeIntegratorOptions = struct(); % immutable
    end
    properties (Dependent, Access = public)
        paramNames
        paramDefaultValues
        paramFixValues
        inLogSpace
        paramBoundLower
        paramBoundUpper
        fixedSpecParamIdxs
        unfixedSpecParamIdxs
        impossibleToFixSpecParamIdxs
        possibleToFixSpecParamIdxs
        modifiableModParamIdxs
        % fixedModParamIdxs
    end
    properties (Access = protected)
        p_paramsSpec % immutable
        p_fixedSpecParamsMask % _mutable_
    end
    properties (Dependent, Access = protected)
        p_nonfixableSpecParamsMask
        p_possibleToFixSpecParamsMask
    end
    methods % Dependent properties
        function ret = get.p_nonfixableSpecParamsMask(self)
            ret = isnan(self.p_paramsSpec.projections);
        end
        function ret = get.p_possibleToFixSpecParamsMask(self)
            ret = ~(self.p_nonfixableSpecParamsMask | self.p_fixedSpecParamsMask);
        end

        function ret = get.fixedSpecParamIdxs(self)
        %FIXEDSPECPARAMIDXS Parameter indices _in the param specification_,
        % which value has been fixed.
        %
            ret = find(self.p_fixedSpecParamsMask);
        end
        function ret = get.unfixedSpecParamIdxs(self)
        %UNFIXEDSPECPARAMIDXS Parameter indices _in the param specification_,
        % which value has not been fixed.
        %
            ret = find(~self.p_fixedSpecParamsMask);
        end
        function ret = get.impossibleToFixSpecParamIdxs(self)
        %IMPOSSIBLETOFIXSPECPARAMIDXS Parameter indices _in the param specification_,
        % which value is not fixed and cannot be fixed.
        %
        % sort([o.impossibleToFixSpecParamIdxs o.possibleToFixSpecParamIdxs])...
        % ==...
        % setdiff(1:numel(o.modifiableModParamIdxs), o.fixedSpecParamIdxs)
        %
            ret = find(~self.p_fixedSpecParamsMask & self.p_nonfixableSpecParamsMask);
        end
        function ret = get.possibleToFixSpecParamIdxs(self)
        %POSSIBLETOFIXSPECPARAMIDXS Parameter indices _in the param specification_,
        % which value has not yet been fixed but can be fixed.
        %
            ret = find(self.p_possibleToFixSpecParamsMask);
        end
% unused
%        function ret = get.fixedModParamIdxs(self)
%        %FIXEDMODPARAMIDXS Parameter indices _in the model_, which value has
%        % been fixed.
%        %
%            ret = self.modifiableModParamIdxs(self.p_fixedSpecParamsMask);
%        end
        function ret = get.modifiableModParamIdxs(self)
        %MODIFIABLEMODPARAMIDXS Parameter indices _in the model_, which value
        % is initially variable. Some of them are then projectable.
        %
            ret = self.p_paramsSpec.idxs;
        end
        function ret = get.paramNames(self)
            ret = self.p_paramsSpec.names;
        end
        function ret = get.paramDefaultValues(self)
            ret = self.p_paramsSpec.values;
        end
        function ret = get.paramFixValues(self)
            ret = self.p_paramsSpec.projections;
        end
        function ret = get.inLogSpace(self)
            ret = self.p_paramsSpec.islog;
        end
        function ret = get.paramBoundLower(self)
            ret = self.p_paramsSpec.bmin;
        end
        function ret = get.paramBoundUpper(self)
            ret = self.p_paramsSpec.bmax;
        end
    end
    methods (Access = protected)
%        % No need to override the default copyElement@matlab.mixin.Copyable;
%        % arrays gonna be anyway deep copied (only on change).
%        function other = copyElement(self)
%            %  shallow copy all
%            other = copyElement@matlab.mixin.Copyable(self);
%            % deep copy mutable properties
%            other.p_fixedSpecParamsMask = self.p_fixedSpecParamsMask;
%        end
        function updateEvalModel(self)
            % set self as a persistent variable for `evalModel` calls
            evalModel(self);
        end
    end
    methods (Access = public)
        function self = EvalModelOptions(mexNames, expData, paramsSpec, varargin)
        %EVALMODELOPTIONS ...
        %
            self.mexNames = mexNames;
            self.expData = expData;
            % try/catch check because it's independent on paramSpec implement.
            % (works for table, struct, and objects)
            try
                paramsSpec.idxs;
                paramsSpec.values;
            catch ME
                error('EVALMODELOPTIONS:InvalidArgument', 'Parameter specification fields ''idxs'' or ''values'' are not set; to get them you can run getIQMparametersProps.');
            end
            self.p_paramsSpec = paramsSpec;
            if ~isempty(varargin)
                self.odeIntegratorOptions = varargin{1};
            end
            self.p_fixedSpecParamsMask = false(numel(paramsSpec.projections),1);
            self.updateEvalModel();
        end

        function ret = fixSpecParamIdxs(self, idxs)
        %FIXSPECPARAMIDXS Fix parameters. Indexing over indices in the params
        % specification. Returns vector type-compatible with the input indices,
        % indicating params that were actually fixed.
        %
            % varying params that can be fixed, will be fixed
            ret = splitIdx(idxs(:), self.p_possibleToFixSpecParamsMask);
            self.p_fixedSpecParamsMask(ret) = true;
            % in case evalModel's persistent ref is unset in the curr worker
            self.updateEvalModel();
        end
% unused (warning: error-prone)
%         function ret = fixPossibleToFixParamIdxs(self, idxs)
%         %FIXPOSSIBLETOFIXIDXS Fix parameters __indexing over possible to
%         % fix parameters__. Returns parameter specification indices of
%         % fixed parameters.
%         %
%             ret = self.possibleToFixSpecParamIdxs(idxs);
%             self.fixSpecParamIdxs(ret);
%         end
        function ret = varySpecParamIdxs(self, idxs)
        %VARYSPECPARAMIDXS Reverse fixing of parameters, i.e. vary them.
        % Same input/output semantics as for the fixSpecParamIdxs method.
        %
            % fixed will be varied
            ret = splitIdx(idxs(:), self.p_fixedSpecParamsMask);
            self.p_fixedSpecParamsMask(ret) = false;
            % in case evalModel's persistent ref is unset in the curr worker
            self.updateEvalModel();
        end
        function ret = isFixedSpecParam(self, idx)
            ret = self.p_fixedSpecParamsMask(idx);
        end
    end
end
