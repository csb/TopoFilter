function [ exit_status, results_fn, elapsed_time ] = templateExperiment( varargin )
    assert(exist('TFmain','file') == 2,...
        'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    %% Initialise
	rng('shuffle');

    fprintf('Program started at %s\n',timestamp());
	tic;


    %% Tunable experiments setting if not provided via function options (+ validation)
    parser = inputParser;
    parser.FunctionName = 'TEMPLATEEXPERIMENT: ';

    % Results file
    addParameter(parser, 'outputFilename', fullfile('./results',...
        sprintf('%s.mat',datestr(now, 'yymmdd_HHMMSS'))), @ischar);

    % Number of runs
    addParameter(parser, 'numRuns', 1, @isposint);
    % Dry run: calc only threshold (and p0, if not given)
    addParameter(parser, 'dryRun', false, @islogical);

    % Sample sizes: not directly options itself, but used below for setting
    % the actual nmont, nelip and nvmin options.
    % Maximum number of cost function evauations in one sampling (roughly).
    addParameter(parser, 'nfeval', 1e2, @isposint);
    % Ratio of nfeval's going respectively into nmont and nelip
    addParameter(parser, 'rfeval', [4 5], @(x) numel(x)==2 && isposint(x));
    % Minimal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmin', 0.1, @isprob);
    % Maximal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmax', 1.9, @(x) isnumeric(x) && (x >= 1));

    % Flag for saving viable points after each sampling
    addParameter(parser, 'saveViablePoints', false, @islogical);

    % Flag indicating if viable points found with the adaptive MCMC method
    % should be dropped.
    addParameter(parser, 'dropMontPoints', false, @islogical);

    % Flags to control flow of the model space search
    % * Recursive search
    addParameter(parser, 'recursive', true, @islogical);
    % * Enumeration level for the viable projections that allows to switch the
    %   heuristic between goals of: a) finding a maximal reduction (0), and
    %   b) finding as many reductions as possible (2)
    addParameter(parser, 'enumLevel', 1, isinrange(2));
    % * Number of (couples of) parameters to combine at most at once during the
    %   initial exhaustive search for the viable projections (`1` = singletons,
    %   `2` = singletons and pairs etc.)
    addParameter(parser, 'exhaustiveRank', 1, @isposint);
    % EXPERIMENTAL, do not use unless you know what you're doing
    addParameter(parser, 'backtrack', false, @islogical);
    addParameter(parser, 'pointsAsOuter', true, @islogical);

    % Parallel computation level
    % 0 - none
    % 1 - over the OAT viability checks in getViableProjections
    % 2 - over projections (in the recursive search) and OAT checks (pre-recursion)
    % 3 - over runs
    addParameter(parser, 'parallelize', 1, isinrange(3));

    % Param space exploration specification
    addParameter(parser, 'paramSpecsFilename', './paramSpecs.txt', @ischar);
    addParameter(parser, 'couplingMatrixFcn', @couplingMatrix, @isf);

    % Init param value
    % 0 - from the paramters specification, or from the model (in that order)
    addParameter(parser, 'p0', 0, @isnumeric);

    % Cost function and extra arguments
    addParameter(parser, 'calcCostFcn', @calcCost, @isf);
    addParameter(parser, 'calcCostFcnArgs', {}, @iscell);
    % Viability threshold function and extra (non-default) arguments
    addParameter(parser, 'calcThresholdFcn', @calcThresholdQuantile, @isf);
    addParameter(parser, 'calcThresholdFcnArgs', {}, @iscell);

    % ODE integrator options
    % options structure with as specified in `IQMPsimulate`
    addParameter(parser, 'odeIntegratorOptions', struct(), @isstruct);
    % number of integration attempts in case of an error; each next with
    % 10-times previous `maxnumsteps` (default: `1e5`)
    addParameter(parser, 'odeIntegratorMaxnumstepsTry', 3, @isposint);

    % Parse the input arguments
    parse(parser, varargin{:});
    p = parser.Results;


    %% Model, experiment and data files
    % SBML (TXT/XML) or IQMmodel (TXT/Matlab) format
    p.modelFilename = './model.txt';
    % IQMexperiment format; string or cell array of string;
    % one experiment expected per each data file
    p.expFilename = './exp.exp';
    % IQMmeasurement format (CSV or XLS); string or cell arrray of string;
    % in case of XLS each sheet (except for first) is treated like a single CSV
    p.expDataFilename = './expData.txt';
    % Indices for data files to use (helpful w/ mult-sheet XLS); [] = all
    p.expDataIdxs = [];

    %% Sample sizes
    % Split nfeval according to given proportions
    nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
    % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
    p.nmont = nfevalv(1);
    % Guiding number of model evaluations in the Ellipsoids-based sampling
    p.nelip = nfevalv(2);
    % Minimum nr of viable parameter samples before (re-)sampling
    p.nvmin = max(1, round(p.pvmin*sum(nfevalv)));
    % Maximum nr of viable parameter samples before sparsifying
    p.nvmax = ceil(p.pvmax*sum(nfevalv));

	%% Run the actual program
    [ runs, viabilityThreshold, settings ] = TFmain(p);

	%% Save results
    results_fn = settings.outputFilename;
    save(results_fn, 'runs', 'viabilityThreshold', 'settings');

	%% Cleanup
    elapsed_time = toc;
    fprintf('Program finished at %s\n',timestamp());

    exit_status = 0;
end
