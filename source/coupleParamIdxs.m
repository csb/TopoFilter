function [coupledParameterIdxsCell, coupledColIdxsCell] = coupleParamIdxs(couplingMatrixFcn, parameterIdxs,colIdxs,d,varargin)
%
assert(numel(parameterIdxs) == numel(colIdxs), 'Invalid input arguments: inconsistent size of both indices vectors.');
    if isempty(varargin)
        rank = 1;
    else
        rank = varargin{1};
    end

    coupling = feval(couplingMatrixFcn, d, parameterIdxs);
    if any(size(coupling) < [d d])
        % expand
        coupling(d,d) = false;
    end
    if any(size(coupling) > [d d])
        warning('Coupling matrix too big - trimming.');
        coupling = coupling(1:d,1:d);
    end
    coupling =  diag(true(d,1)) | coupling;

    %% create cell array w/ specified coupling
    loc_coupling = coupling(parameterIdxs,parameterIdxs);
    % remove duplicates due to both ways coupling
    loc_coupling = unique(loc_coupling,'rows');
    % create all unique loc_coupling rows combinations up to the given rank
    n0 = size(loc_coupling,1);
    rank = min(rank, n0); % can only combine rows up to n0
    if n0 > 0 && rank > 1
        loc_coupling_ca = cell(rank,1);
        loc_coupling_ca{1} = loc_coupling;
        for r=2:rank
            I = nchoosek(1:n0, rank);
            loc_coupling_r_ca = arrayfun(@(i) max(loc_coupling(I(i,:),:)), 1:size(I,1), 'UniformOutput', false);
            loc_coupling_ca{r} = unique(vertcat(loc_coupling_r_ca{:}),'rows');
        end
        loc_coupling = unique(vertcat(loc_coupling_ca{:}),'rows');
    end
    n = size(loc_coupling,1);
    coupledParameterIdxsCell = cell(1,n);
    coupledColIdxsCell = cell(1,n);
    for i=1:n
        % Cosmetics: revert order after sorting of logical vectors by unique
        %            (to agree w/ input indices vectors)
        icouple = loc_coupling(n-i+1,:);
        coupledParameterIdxsCell{i} = parameterIdxs(icouple);
        coupledColIdxsCell{i} = colIdxs(icouple);
    end
end
