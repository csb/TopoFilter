function [ cost, paramValues ] = evalModelDirect( varargin )
%EVALMODELDIRECT Simplified version of the evalModel function, for a direct
% evaluation of the experiment cost. Initialise first:
%
% evalModelDirect(mexNames, expData, paramNames, islog, fixedParamValuesTail)
%

persistent fdat;

if ~isnumeric(varargin{1})
    fdat.debugging = (debuglevel > 1);

    fdat.mexNames = varargin{1};
    fdat.expData = varargin{2};
    fdat.paramNames = varargin{3};
    fdat.islog = varargin{4};
    fdat.fixedParamValuesTail = varargin{5};

    fdat.nPar = numel(fdat.paramNames);
    fdat.nParFix = numel(fdat.fixedParamValuesTail);
    assert(fdat.nParFix <= fdat.nPar, ...
        'Number of fixed parameter values greater than number of parameter names.');
    assert((fdat.nPar - fdat.nParFix) == numel(fdat.islog), ...
        'Size of a flag of logarithmic scale for variable parameters is inconsistent with sizes of all parameters names cell and fixed parameters values.');

    cost = [];
    paramValues = [];

else
    assert(~isempty(fdat), 'First, setup the persistent data for the function.');

    variableParamValuesHead = varargin{1};
    maxT = fdat.expData.maxT;

    variableParamValuesHead = variableParamValuesHead(:);
    paramValues = vertcat(variableParamValuesHead, fdat.fixedParamValuesTail);
    paramValues(fdat.islog) = 10.^paramValues(fdat.islog);
    for i = numel(fdat.mexNames):-1:1
        nsols(i) = IQMPsimulate(fdat.mexNames{i}, maxT(i), [], fdat.paramNames, paramValues);
    end
    cost = fdat.expData.cost(nsols, numel(variableParamValuesHead));


    if fdat.debugging
        fprintf('[DEBUG] evalModelDirect(%s) = %f.\n', mat2str(paramValues), cost);
    end

end

end
