function [ mexFn, cleanupObj ] = mexModel( model, settings )
%MEXMODEL Create mex versions of the model for simulation.
%
%    Returns a callable MEX name (i.e. w/o folder path and w/o the extension),
%    which consists of the given name prefix and a unique nuermic suffix (clock
%    tick in milliseconds)
%
%    Moreover a cleanup handle is returned as a second output and it has to be
%    assigned, othwerwise MEX will be deleted on exit from this function.
%
%   `settings` struct fields used:
%       `modelFilename` If set as a string, use as the file name part as
%                       a prefix for the MEX file. Otherwise, model name is
%                       used.
%       `parallelize`   If true ,attach mex file to a `gcp` pool.
%

    if nargin < 2
        settings = struct();
    end

    if ~(ischar(model) && exist(model,'file') == 3)
        % Get mex file name w/o ext
        if isfield(settings, 'modelFilename') && ischar(settings.modelFilename)
            [~,mfn] = fileparts(settings.modelFilename);
        else
            modstruct = IQMstruct(model);
            mfn = modstruct.name;
        end
        % Create mex versions of the model for simulation
        mexFn = genvarname(sprintf('%d_%s',clockStamp(),mfn));
        IQMmakeMEXmodel(model,mexFn,0);

        mexPath = fullfile(pwd,mexEqfn(mexFn));
        cleanupObj = onCleanup(@() deleteIfExists(mexPath));
    else
        mexFn = model;
    end

    % make mex file available for all workers
    if isfield(settings, 'parallelize') && (settings.parallelize > 0)
        pool = gcp;
        addAttachedFiles(pool, mexEqfn(mexFn));
    end

end

function [ ret ] = mexEqfn(name)
%MEXEQFN MEX extension qualified file name.
    ret = sprintf('%s.%s', name, mexext);
end

function deleteIfExists(fn)
    if (exist(fn,'file'))
        delete(fn);
    end
end
