classdef(Abstract=true) CachableProperties < hgsetget % in future use: matlab.mixin.SetGet (>=R2014b)
% Usage:
%     classdef A < CachableProperties
%         properties (Access = ?CachableProperties)
%             %cache_myCellArray = {};
%             cache_myArray = [];
%         end
%         methods (Access = protected)
%             function ret = computeMyArray(obj)
%                 ret = randn(5,8);
%             end
%         end
%         methods
%             function ret = myArray(obj, varargin)
%                 ret = obj.getCacheProperty('cache_myArray', @obj.computeMyArray, varargin{:});
%             end
%         end
%    end
%
%    a = A(...)
%    a.myArray
%    a.myArray([1 end], [1 end])
%    a.myArray('-f')
%    a.myArray('--force', [1 end], [1 end]);
%
    %
    methods (Access = protected)
        %
        function value = getCacheProperty(obj, propName, fHandle, varargin)
        %GETCACHENUMPROPERTY Get a cached property value, or compute and cache if cache
        %  isempty or if optional string '-f' or '--force' is passed as a next argument.
        %  If further arguments are passed, e.g. indices, then apply them to the
        %  property value.
        %
        %  This function is intended to be used within object functions that mimmick
        %  regular properties, but are computed on demand.
        %
        %  Examples:
        %
        %       % compute or get from p_vector cache property of object obj
        %       getCacheProperty(obj, 'p_vector', @obj.computeVector)
        %       % compute and cache p_vector property, independent of the previous content
        %       getCacheProperty(obj, 'p_vector', @obj.computeVector, 'force')
        %
        %       % after computing and caching or retrieving 'p_matrix' get first column
        %       getCacheProperty(obj, 'p_matrix', @obj.computeMatrix, :, 1)
        %       % after computing and caching 'p_matrix' get the first column
        %       getCacheProperty(obj, 'p_matrix', @obj.computeMatrix, 'force', :, 1)
        %
        %  Remark: currently, if the cached property computes always to an empty value
        %          it will be re-computed during each call. This can be circumvented
        %          by keeping separate flags for cache properties.
        %
            %
            p = inputParser; % instead of few if's
            addRequired(p, 'obj', @isobject);
            addRequired(p, 'propName', @ischar);
            addRequired(p, 'fHandle', @isfhandle); % change fHandle test and call (to feval) if string name allowed
            parse(p, obj, propName, fHandle);

            if ~(isprop(obj,propName))
                error(sprintf('Cache field ''%s'' does not exist.',propName));
            end

            if (nargin > 3) && ischar(varargin{1}) && any(strcmp({'-f','--force'},lower(varargin{1})))
                force = true;
                pevalArgs = varargin(2:end);
            else % no force flag, but possibly some property evaluation arguments
                force = false;
                pevalArgs = varargin;
            end


            if force || isempty(get(obj,propName))
                set(obj, propName, fHandle());
            end
            value = get(obj,propName);
            if ~isempty(pevalArgs)
                value = value(pevalArgs{:});
            end
        end
    end
end
