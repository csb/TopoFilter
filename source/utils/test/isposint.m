function [bool] = isposint(x)
%ISPOPSINT Test if `x` is a positive integer.
%
%    Note: `Inf` counts as a positive integer.
%
    bool = isint(x) & (x > 0);

end
