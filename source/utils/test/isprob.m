function [bool] = isprob(x)
%ISPROB Test if `x` is a number in [0,1] range (a probability value).
%
% Examples:
%    isprob([0.99 1 1.01]) % ans == [1 1 0]
%    isprob({0.99 1}) % ans == 0
%
    bool = all(isnumeric(x));
    if bool
        bool = (0 <= x & x <= 1);
    end
end
