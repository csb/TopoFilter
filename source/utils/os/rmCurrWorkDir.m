function [ ] = rmCurrWorkDir( newWorkDir, rmPath )
%RMCURRWORKDIR remove current working directory from the file system and
%   from the MATLAB path; cd into `newWorkDir`.
%
% Attributes:
%   newWorkDir (str): Path to the new working directory.
%   rmPath (bool): Optional boolean flag indicating wether or not to remove
%       new working directory from the MATLAB path (it will be still
%       searched for as a current working directory); default value:
%       `false`.
%
    if nargin < 2 || isempty(rmPath)
      rmPath = false;
    end

    currWorkDir = pwd;
    cd(newWorkDir);
    if rmPath
        rmpath(genpath(newWorkDir));
    end
    rmdir(currWorkDir, 's');

end

