function [C, ia, ic] = setRmCols(A, I, varargin)
%SETRMCOLS Return set of rows (no duplicates) of matrix A, left after removing
% columns I. The resulting matrix by default has sorted rows. Links between rows
% of the input and output matrices are given via indices vectors ia and ic.
%
% Inputs:
%      A (numeric): matrix to remove columns from
%      I (posint or logical): indices of columns to remove
%
%      varargin
%          setOrder (string): "stable" or "sorted" (default)
%
    if nargin < 3
        setOrder = 'stable';
    else
        setOrder = varargin{1};
        if ~ischar(setOrder) || ~any(strcmp(setOrder, {'stable', 'sorted'}))
            error('SETPROJECTCOLS:InvalidArgument','Invalid input argument: setOrder.');
        end
    end
    B = A(:,I);
    [C, ia, ic] = unique(B, 'rows', setOrder);
end
