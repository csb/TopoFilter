function [ret] = debuglevel(varargin)
%DEBUGLEVEL Set or get debugging level. If no argument is given than get, if any
%  is given then set to a given integer level (only first argument is used).
%
    persistent DEBUG
    if nargin > 0
        ret = varargin{1};
        if (~isint(ret))
            error('DEBUGLEVEL:IllegalArgument','Expecting integer-valued debuging level.');
        end
        DEBUG = ret;
    else
        if isempty(DEBUG), DEBUG=0; end
        ret = DEBUG;
    end
end
