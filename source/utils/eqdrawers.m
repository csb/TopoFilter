function d = eqdrawers(n, k)
%EQDRAWERS Partition integer n into k-elements integer vector d , where each
%  element is equal either ceil(n/k) or floor(n/k) and sum(d) == n.
%
%  Note: The plus-one parts always come first.
%
    d = zeros(1,k);
    if k < 1, return; end
    d(1:mod(n,k)) = 1;
    d = d + floor(n/k);
    assert(sum(d) == n && numel(d) == k);
end
