function ret = mapAllCell(varargin)
%MAPCELL Recurisvely map all non-cell array elements of a cell-array.
%
%  mapCell(cellArray, fHandlerOrName, ... function args)
%
    ca = varargin{1};
    fun = varargin{2};
    ret= cell(size(ca));
    for i=1:numel(ca)
        ce = ca{i};
        if(~iscell(ce))
            ret{i} = feval(fun, ce, varargin{3:end});
        else
            ret{i} = mapAllCell(ce, fun, varargin{3:end});
        end
    end
end
