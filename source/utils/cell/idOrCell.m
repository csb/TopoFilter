function out = idOrCell(in)
%IDORCELL Return input arg if it is a cell array, otherwise wrap it into
% a singleton cell array.
%

    if ~iscell(in)
        out = {in};
    else
        out = in;
    end

end
