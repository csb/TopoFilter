function [ nloglike ] = mvnnloglike( x, invS, varargin )
%MVNNLOGLIKE Compute the negative log-likelihood of the centralised multivariate
%  normal distribution at x (vector). Requires inverse of the covariance matrix
%  invS.
%
%  For the case of indpendent marginals, inverse of the covariance matrix invS
%  can be given as a vector of reciprocals of individual variances; then
%      invS == diag(s)
%

n = numel(x);
% if given only weights per point, use the diagonal covariance matrix
if (size(invS,1) == 1 || size(invS,2) == 1)
    invS = diag(invS);
end
if ~(issymmetric(invS) && n == size(invS,1))
    error('MVNNLOGLIKE:IncompatibleDimensions', 'Covariance matrix inverse is either not square symmetric, or it''s size does not agree with the dimension of the point (%d by %d vs. %d)',size(S,1),size(S,2),n);
end
% if not given, compute logDetS = log(1/det(invS)) = -log(det(invS))
if isempty(varargin)
    logDetS = -log(det(invS));
else
    logDetS = varargin{1};
end

nloglike = 0.5*(n*log(2*pi)+logDetS+transpose(x)*invS*x);

end
