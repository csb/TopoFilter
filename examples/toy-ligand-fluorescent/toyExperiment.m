function [ exit_status, results_fn, elapsed_time ] = toyExperiment( varargin )

    assert(exist('TFmain') == 2, 'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    %% Initialise
	rng('shuffle');

    fprintf('Program started at %s\n',timestamp());
	tic;


    %% Tunable experiments setting if not provided via function options (+ validation)
    parser = inputParser;
    parser.FunctionName = 'TOYEXPERIMENT: ';

    % Results file
    addParameter(parser, 'outputFilename', fullfile('./results',...
        sprintf('%s.mat',datestr(now, 'yymmdd_HHMMSS'))), @ischar);
    % Number of runs
    addParameter(parser, 'numRuns', 4, @isposint);
    addParameter(parser, 'dryRun', false, @islogical); % calc only threshold (and p0, if not given)


    % Sample sizes: not directly options itself, but used below for setting
    % the actual nmont, nelip and nvmin options.
    % Maximum number of cost function evauations in one sampling (roughly).
    addParameter(parser, 'nfeval', 1e3, @isposint);
    % Ratio of nfeval's going respectively into nmont and nelip
    addParameter(parser, 'rfeval', [4 5], @(x) numel(x)==2 && all(isposint(x)));
    % Minimal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmin', 0.1, @isprob);
    % Maximal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmax', 1.9, @(x) isnumeric(x) && (x >= 1));

    % Flag for saving viable points after each sampling
    addParameter(parser, 'saveViablePoints', false, @islogical);

    % Flag indicating if viable points found with the adaptive MCMC method
    % should be dropped.
    addParameter(parser, 'dropMontPoints', false, @islogical);

    % Flags to control flow of the model space search
    % Flags to control flow of the model space search
    % * Recursive search
    addParameter(parser, 'recursive', true, @islogical);
    % * Enumeration level for the viable projections that allows to switch the
    %   heuristic between goals of: a) finding a maximal reduction (0), and
    %   b) finding as many reductions as possible (2)
    addParameter(parser, 'enumLevel', 2, isinrange(2));
    % * Number of (couples of) parameters to combine at most at once during the
    %   initial exhaustive search for the viable projections (`1` = singletons,
    %   `2` = singletons and pairs etc.)
    addParameter(parser, 'exhaustiveRank', 1, @isposint);
    % EXPERIMENTAL, do not use unless you know what you're doing
    addParameter(parser, 'backtrack', true, @islogical);

    % Parallel computation level
    addParameter(parser, 'parallelize', 0, isinrange(3));
    % Param space exploration specification
    addParameter(parser, 'paramSpecsFilename', './paramSpecs_4a.txt', @ischar);
    addParameter(parser, 'couplingMatrixFcn', 'toyCouplingMatrix', @isf);

    % Model file
    addParameter(parser, 'modelFilename', './model.txt', @ischar);

    % Init param value
    addParameter(parser, 'p0', 0, @isnumeric); % 0 = from the param spec or model

    % ODE integration options
    addParameter(parser, 'odeIntegratorSolNrTimepoints', 1, @isint); % 0 = IQMPsimulate default

    % Parse the input arguments
    parse(parser, varargin{:});
    p = parser.Results;

    % Viability threshold legacy method
    p.calcThresholdFcn = @calcThresholdSd;

    %% Model, experiment and data files
    p.expFilename = './exp-SteadyState.exp';
    p.expDataFilename = './expData.xls';

    %% Sample sizes
    % Split nfeval according to given proportions
    nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
    % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
    p.nmont = nfevalv(1);
    % Guiding number of model evaluations in the Ellipsoids-based sampling
    p.nelip = nfevalv(2);
    % Minimum nr of viable parameter samples before (re-)sampling
    p.nvmin = max(2, round(p.pvmin*sum(nfevalv)));
    % Maximum nr of viable parameter samples before sparsifying
    p.nvmax = ceil(p.pvmax*sum(nfevalv));

    %% Run the actual program
    [ runs, viabilityThreshold, settings ] = TFmain(p);


	%% Save results
    results_fn = settings.outputFilename;
    save(results_fn, 'runs', 'viabilityThreshold', 'settings');


    %% Cleanup
    elapsed_time = toc;
    fprintf('Program finished at %s\n',timestamp());

    exit_status = 0;
end
