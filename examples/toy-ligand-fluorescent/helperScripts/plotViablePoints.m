function plotViablePoints(viablePoints)
	figure;
	subplot(131);
	scatter(viablePoints(:,1), viablePoints(:,2),'.');
	subplot(132);
	histogram(viablePoints(:,1),20);
	subplot(133);
	histogram(viablePoints(:,2),20);

	set(gcf,'Position',[50, 100, 1200, 400])
end