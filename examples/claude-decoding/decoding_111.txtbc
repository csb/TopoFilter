********** MODEL NAME
Decoding 111

********** NOTES
% big model
% no model of MAPK pathway. only Fus3ppn input (will be determined later by what we can achieve experimentally)
% TF phosphorylation by Fus3 triggers expression
% no mRNA intermediate state.
% fusion promoters for OR gates.
% adding leakiness

********** MODEL STATE INFORMATION
TF1(0)    = 0        % transcriptional activator 
TF1p(0)   = 0        % phosphorylated TF (active for Ptf1 = 1)
TF2(0)    = 0        % repressor or transcription factor
TF3(0)    = 0        % repressor or transcription factor
output(0) = 0        % synthetic output. ex: GFP 
Fus1(0)   = 0        % Pheromone responsive elements required for mating


********** MODEL PARAMETERS
alpha     = 0
input     = 0
ind_time  = 20         % induction time
d         = 0.005      % min-1 degradation rate of proteins
kLfus1    = 0.01       % leakiness relative to max expression
kfus1     = 10         % nM min-1 = 500nM* 4e-6 s-1 max mRNA synthesis induced by Fus3pp (natural Fus1 promoter)
Kmfus1    = 500        % nM mRNA synthesis induced by Fus3p Km value (natural Fus1 promoter)
kp        = 0.5        % min-1 phosphorylation rate of TF by Fus3pp
kd        = 10         % degradation rate fold induced by Fus3pp
Kmp       = 300        % nM phosphorylation Km of TF by Fus3pp
Ptf1      = 1          % for activators, Fus3 phosphorylation dependence. 1 = TF phopshorylated by Fus3, and TFp active only. 0 = TF active only.
Atf2      = 1          % TF1 type. 1 = activator. 0 = repressor.
Atf3      = 1          % TF1 type. 1 = activator. 0 = repressor.
ktf1      = 5          % nM min-1 = 500nM* 4e-6 s-1 max TF1 synthesis
kLtf1     = 0.01       % percent leakiness
Km11      = 1e6       % nM TF1 synthesis activation by TF1 Km value 
Km12      = 1e6       % nM TF1 synthesis activation by TF2 Km value  
Km13      = 1e6       % nM TF1 synthesis activation by TF3 Km value  
Km1fus3   = 1e6       % nM TF1 synthesis activation by Fus3ppn Km value
ktf2      = 5          % nM min-1 = 500nM* 4e-6 s-1 max TF2 synthesis
kLtf2     = 0.01       % percent leakiness
Km21      = 1e6       % nM TF2 synthesis activation by TF1 Km value 
Km22      = 1e6       % nM TF2 synthesis activation by TF2 Km value  
Km23      = 1e6       % nM TF2 synthesis activation by TF3 Km value  
Km2fus3   = 1e6       % nM TF2 synthesis activation by Fus3ppn Km value
ktf3      = 5          % nM min-1 = 500nM* 4e-6 s-1 max TF2 synthesis
kLtf3     = 0.01       % percent leakiness
Km31      = 1e6       % nM TF3 synthesis activation by TF1 Km value 
Km32      = 1e6       % nM TF3 synthesis activation by TF2 Km value  
Km33      = 1e6       % nM TF3 synthesis activation by TF3 Km value  
Km3fus3   = 1e6       % nM TF3 synthesis activation by Fus3ppn Km value
kout      = 10         % nM min-1 = 500nM* 4e-6 s-1 max output synthesis
kLout     = 0.01       % percent leakiness
Kmout1    = 1e6        % nM output synthesis activation by TF1 Km value 
Kmout2    = 1e6       % nM output synthesis activation by TF2 Km value  
Kmout3    = 1e6       % nM output synthesis activation by TF3 Km value  
Kmoutfus3 = 1e6       % nM output synthesis activation by Fus3ppn Km value

                  
********** MODEL VARIABLES
Fus3pp =  piecewiseIQM(490*alpha + input*490*exp(-(time-(ind_time+30))*0.02) + 10,ge(time,ind_time + 30),490* (alpha + input) + 10,ge(time,ind_time),10);
TF1on = Ptf1*TF1p + (1-Ptf1)*(TF1+TF1p)
% Ra1 = output/400 %should be low for input induction
% Ra2 = 2000/Fus1
% Ri1 = 2000/output
% Ri2 = Fus1/400
% MM3 = piecewiseIQM((TF)^2/((TF)^2+Km3^2),ge(Km3,1), 1)
% MM4 = piecewiseIQM((TF)^2/((TF)^2+Km4^2),ge(Km4,1), 1)
% MM5 = piecewiseIQM((TF)^2/((TF)^2+Km5^2),ge(Km5,1), 1)


********** MODEL REACTIONS
        
                     => Fus1                                      : RFus1% transcription, Fus1 promoter
                  vf = kfus1*(kLfus1 + (1-kLfus1)*((Fus3pp/Kmfus1)^2)/((Fus3pp/Kmfus1)^2+1))    
                     
                     => TF1                                       : RTF1% transcription of TF1
              	  vf = ktf1*(kLtf1 + (1-kLtf1)*((Fus3pp/Km1fus3)^2 +  (TF1on/Km11)^2 + Atf2*(TF2/Km12)^2 +  Atf3*(TF3/Km13)^2)/(1 + (Fus3pp/Km1fus3)^2 +  (TF1on/Km11)^2 + Atf2*(TF2/Km12)^2) +  Atf3*(TF3/Km13)^2)*1/(1 + (1-Atf2)*(TF2/Km12)^2)*1/(1 + (1-Atf3)*(TF3/Km13)^2 ) 
                  
                     => TF2                                       : RTF2% transcription of TF2
              	  vf = ktf2*(kLtf2 + (1-kLtf2)*((Fus3pp/Km2fus3)^2 +  (TF1on/Km21)^2 + Atf2*(TF2/Km22)^2 +  Atf3*(TF3/Km23)^2)/(1 + (Fus3pp/Km2fus3)^2 +  (TF1on/Km21)^2 + Atf2*(TF2/Km22)^2) +  Atf3*(TF3/Km23)^2)*1/(1 + (1-Atf2)*(TF2/Km22)^2)*1/(1 + (1-Atf3)*(TF3/Km23)^2 ) 
  
                     => TF3                                       : RTF3% transcription of TF3
              	  vf = ktf3*(kLtf3 + (1-kLtf3)*((Fus3pp/Km3fus3)^2 +  (TF1on/Km31)^2 + Atf2*(TF2/Km32)^2 +  Atf3*(TF3/Km33)^2)/(1 + (Fus3pp/Km3fus3)^2 +  (TF1on/Km31)^2 + Atf2*(TF2/Km32)^2) +  Atf3*(TF3/Km33)^2)*1/(1 + (1-Atf2)*(TF2/Km32)^2)*1/(1 + (1-Atf3)*(TF3/Km33)^2 ) 

                     => output                                    : Rout% transcription of output
              	  vf = kout*(kLout + (1-kLout)*((Fus3pp/Kmoutfus3)^2 +  (TF1on/Kmout1)^2 + Atf2*(TF2/Kmout2)^2 +  Atf3*(TF3/Kmout3)^2)/(1 + (Fus3pp/Kmoutfus3)^2 +  (TF1on/Kmout1)^2 + Atf2*(TF2/Kmout2)^2) +  Atf3*(TF3/Kmout3)^2)*1/(1 + (1-Atf2)*(TF2/Kmout2)^2)*1/(1 + (1-Atf3)*(TF3/Kmout3)^2 ) 

            TF1     => TF1p                                       : P1% phosphorylation by Fus3pp
              	  vf = kp*TF1/(TF1+Kmp)*Fus3pp   
                  
            Fus1     =>                                           : DFus1% protein degradation
                  vf = d*Fus1
     
            TF1     =>                                            : DTF1% protein degradation
                  vf = d*TF1 
            
            TF1p    =>                                            : DTF1p% protein degradation
                  vf = d*TF1p + (1-Ptf1)*(kd-1)*d*TF1p      

            TF2     =>                                            : DTF2% protein degradation
                  vf = d*TF2                
 
            TF3     =>                                            : DTF3% protein degradation
                  vf = d*TF3      
                  
            output   =>                                           : Dout% protein degradation
                  vf = d*output                  
                  
********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL MATLAB FUNCTIONS
