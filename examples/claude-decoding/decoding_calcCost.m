function [ cost, residuals ] = decoding_calcCost( x, y, npar, varargin )

if (debuglevel > 1)
    fprintf('[DEBUG] decoding_calcCost, npar = %d.\n',npar);
end

% just in case - flatten both
x = x(:);
y = y(:);
n = numel(x);
if n ~= numel(y)
    error('DECODING_CALCCOST:IncompatibleDimensions', 'Number of observed data does not agree with number of measured data (%d vs. %d)',n,numel(y));
end

residuals = x - y;

assert(numel(x)==6, 'Expecting 6 simulated values.');
% Attention: x should be in the order agreeing with expFilename, but double check that in a case where you know what values to expect (e.g. just print them out).
ctrl = x(1:2);
alpha = x(3:4);
input = x(5:6);
alpha_out = alpha(1);
alpha_fus1 = alpha(2);
ctrl_out = ctrl(1);
% ctrl_fus1 = ctrl(2);
input_out = input(1);
% input_fus1 = input(2);
cost = (1/10*alpha_out/ctrl_out)^2 + (10*alpha_out/input_out)^2 + (10*ctrl_out/input_out)^2 + (0.5*alpha_fus1/input_out)^2 + (10*ctrl_out/alpha_fus1)^2;

end
