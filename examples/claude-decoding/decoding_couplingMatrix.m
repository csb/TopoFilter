function C = couplingMatrix(d, parameterIdxs)
%COUPLINGMATRIX Return logical d x d matrix C which specifies coupling of
% projections of parameters in the following form:
%    C(i,j) = if param i is projected then project also param j
%
% This is a default implemenation, with no copuling. Speficy yours via
% copulingMatrixFcn option of the experiment.
%
% Input vector parameterIdxs is a set of currently projectable parameter indices
% (rows in parameter specification). It allows for dynamic definitions (see
% example below). It is a subset of all modifiable parameter indices 1:d. Its
% complement to the vector 1:d is a set of either nonprojectable or already
% projected indices.
%
% Example 1: Symmetric coupling
% Always project together params in rows 1 and 2 in params specification:
%     C(1,2) = true;
%     C(2,1) = true;
%
% Example 2: Assymetric coupling
% When param in row 2 in params specification (think: v_max) is projected
% then project also param in row 1 in params specification (think: Km),
% but not the other way around:
%     C(2,1) = true;
%
% Example 3: Dynamic coupling
% If 1 (Vmax) is projected than also project 2, 3, 4 (Km1, Km2, Km3);
% if all Kmi are projected (think: to infinity, s.t. Xi/Kmi = 0 in the
% regulatory part of reaction rate), then project also Vmax.
%     C(1,2) = true;
%     C(1,3) = true;
%     C(1,4) = true;
%     C(4,1) = ~any(ismember([2 3],parameterIdxs));
%     C(3,1) = ~any(ismember([2 4],parameterIdxs));
%     C(2,1) = ~any(ismember([3 4],parameterIdxs));
%

    % initialise: no coupling
    C = diag(true(d,1));
    
    C(1,2) = true;
    C(2,1) = true;    
    C(1,3) = true;
    C(1,4) = true;
    C(1,5) = true;
    C(1,6) = true;
    C(3,1) = ~any(ismember([4 5 6],parameterIdxs));
    C(4,1) = ~any(ismember([3 5 6],parameterIdxs));
    C(5,1) = ~any(ismember([3 4 6],parameterIdxs));
    C(6,1) = ~any(ismember([3 4 5],parameterIdxs));
    
    C(7,8) = true;
    C(8,7) = true;
    C(7,9) = true;
    C(7,10) = true;
    C(7,11) = true;
    C(7,12) = true;
    C(9,7) = ~any(ismember([10 11 12],parameterIdxs));
    C(10,7) = ~any(ismember([9 11 12],parameterIdxs));
    C(11,7) = ~any(ismember([9 10 12],parameterIdxs));
    C(12,7) = ~any(ismember([9 10 11],parameterIdxs));
    
    C(13,14) = true;
    C(14,13) = true;
    C(13,15) = true;
    C(13,16) = true;
    C(13,17) = true;
    C(13,18) = true;
    C(15,13) = ~any(ismember([16 17 18],parameterIdxs));
    C(16,13) = ~any(ismember([15 17 18],parameterIdxs));
    C(17,13) = ~any(ismember([15 16 18],parameterIdxs));
    C(18,13) = ~any(ismember([15 16 17],parameterIdxs));
    
    C(19,20) = true;
    C(20,19) = true;
    C(19,21) = true;
    C(19,22) = true;
    C(19,23) = true;
    C(19,24) = true;    
    C(21,19) = ~any(ismember([22 23 24],parameterIdxs));
    C(22,19) = ~any(ismember([21 23 24],parameterIdxs));
    C(23,19) = ~any(ismember([21 22 24],parameterIdxs));
    C(24,19) = ~any(ismember([21 22 23],parameterIdxs));

end
