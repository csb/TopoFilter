function [ exit_status, results_fn, elapsed_time ] = decoding_main( varargin )

    assert(exist('TFmain') == 2, 'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    %% Initialise
	rng('shuffle');

    fprintf('Program started at %s\n',timestamp());
	tic;

    start = datestr(now, 'yymmdd_HHMMSS');

    %vAll = [000 010 011 100 110];
    vAll = [011];
    n = numel(vAll);
    results_fn = cell(n,1);
    for i = 1:n
        v = vAll(i);
        outDirname = fullfile('results', start, sprintf('decoding_%03d', v));
        if exist(outDirname, 'dir') == 0
            mkdir(outDirname);
        end
        outBasename = fullfile(outDirname, sprintf('%s',datestr(now, 'yymmdd_HHMMSS')));

        %% Tunable experiments setting if not provided via function options (+ validation)
        parser = inputParser;

        parser.FunctionName = 'TOPO_DECODING: ';

        % Results file
        addParameter(parser, 'outputFilename', sprintf('%s.mat',outBasename), @ischar);

        % Number of runs
        addParameter(parser, 'numRuns', 1, @isposint);
        % Dry run: calc only threshold (and p0, if not given)
        addParameter(parser, 'dryRun', false, @islogical);

        % Sample sizes: not directly options itself, but used below for setting
        % the actual nmont, nelip and nvmin options.
        % Maximum number of cost function evauations in one sampling (roughly).
        addParameter(parser, 'nfeval', 1e2, @isposint);
        % Ratio of nfeval's going respectively into nmont and nelip
        addParameter(parser, 'rfeval', [4 5], @(x) numel(x)==2 && all(isposint(x)));
        % Minimal size of viable points sample as a proportion of nfeval
        addParameter(parser, 'pvmin', 0.1, @isprob);
        % Maximal size of viable points sample as a proportion of nfeval
        addParameter(parser, 'pvmax', 1.9, @(x) isnumeric(x) && (x >= 1));

        % Flag for saving viable points after each sampling
        addParameter(parser, 'saveViablePoints', true, @islogical);

        % Flags to control flow of the model space search
        % * Recursive search
        addParameter(parser, 'recursive', true, @islogical);
        % * Enumeration level for the viable projections that allows to switch the
        %   heuristic between goals of: a) finding a maximal reduction (0), and
        %   b) finding as many reductions as possible (2)
        addParameter(parser, 'enumLevel', 1, isinrange(2));
        % * Number of (couples of) parameters to combine at most at once during the
        %   initial exhaustive search for the viable projections (`1` = singletons,
        %   `2` = singletons and pairs etc.)
        addParameter(parser, 'exhaustiveRank', 1, @isposint);

        % Parallel computation level
        % 0 - none
        % 1 - over the OAT viability checks in getViableProjections
        % 2 - over projections (in the recursive search) and OAT checks (pre-recursion)
        % 3 - over runs
        addParameter(parser, 'parallelize', 1, isinrange(3));
        %addParameter(parser, 'parallelize', 0, isinrange(3));

        % Param space exploration specification
        addParameter(parser, 'paramSpecsFilename', './paramSpecs.txt', @ischar);
        addParameter(parser, 'couplingMatrixFcn', @decoding_couplingMatrix, @isf);

        % Init param value
        % 0 - from the paramters specification, or from the model (in that order)
        loaded = load(sprintf('p%03d', v));
        addParameter(parser, 'p0', loaded.p0, @isnumeric);

        addParameter(parser, 'calcCostFcn', @decoding_calcCost, @isf);
        addParameter(parser, 'calcThresholdFcn', @decoding_calcThreshold, @isf);

        % ODE integrator options
        % options structure with as specified in `IQMPsimulate`
        simopts.reltol = 1e-6;
        simopts.abstol = 1e-6;
        simopts.maxnumsteps = 1e6;
        addParameter(parser, 'odeIntegratorOptions', simopts, @isstruct);
        % number of integration attempts in case of an error; each next with
        % 10-times previous `maxnumsteps` (default: `1e5`)
        addParameter(parser, 'odeIntegratorMaxnumstepsTry', 1, @isposint);


        % Parse the input arguments
        parse(parser, varargin{:});
        p = parser.Results;


        %% Model, experiment and data files
        % SBML (TXT/XML) or IQMmodel (TXT/Matlab) format
        p.modelFilename = sprintf('./decoding_%03d.txtbc', v);
        % IQMexperiment format; string or cell array of string;
        % one experiment expected per each data file
        p.expFilename = {'./exp-control.exp','./exp-alpha.exp','./exp-input.exp'};
        % IQMmeasurement format (CSV or XLS); string or cell arrray of string;
        % in case of XLS each sheet (except for first) is treated like a single CSV
        p.expDataFilename = './expData.xls';
        % Indices for data files to use (helpful w/ mult-sheet XLS); [] = all
        p.expDataIdxs = [];

        %% Sample sizes
        % Split nfeval according to given proportions
        nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
        % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
        p.nmont = nfevalv(1);
        % Guiding number of model evaluations in the Ellipsoids-based sampling
        p.nelip = nfevalv(2);
        % Minimum nr of viable parameter samples before (re-)sampling
        p.nvmin = max(1, round(p.pvmin*sum(nfevalv)));
        % Maximum nr of viable parameter samples before sparsifying
        p.nvmax = ceil(p.pvmax*sum(nfevalv));

        %% Run the actual program
        diary(sprintf('%s.log', outBasename));
        disp('decoding_main');
        disp('=============');

        [ runs, viabilityThreshold, settings ] = TFmain(p);

        diary('off');

        %% Save results
        results_fn{i} = settings.outputFilename;
        save(results_fn{i}, 'runs', 'viabilityThreshold', 'settings');

    end

    %% Cleanup
    elapsed_time = toc;
    exit_status = 0;
    fprintf('Program finished at %s\n',timestamp());
end
