function [ varargout ] = evalModel( argin )
%EVALMODEL Summary of this function goes here
%   Detailed explanation goes here

persistent persistentData

if isstruct(argin)
    persistentData = argin;

else
    %prepare the simulator for mex files
    errVal = 1e-12;             %value for relative and absolute tolerance
    options.reltol = errVal;    %relative tolerance
    options.abstol = errVal;    %absolute tolerance
    options.maxnumsteps = 1e6;  %maximum number of steps

    % read out parameter names, default values, initial conditions, time
    % points at which measurements were taken and indices of observables
    paramNames = persistentData.paramNames;
    paramDefaultValues = persistentData.paramDefaultValues;
    % FIXME will not work now => needs .model property in EvalModelOptions
    [~,~,ICs] = IQMstates(persistentData.model);
    timePoints = persistentData.expData.timePoints;
    maxT = max(timePoints);


    %get the function input
    newParamValues = argin;

    % define new parameter point
    paramValues = paramDefaultValues;
    paramValues(persistentData.modifiableModParamIdxs) = newParamValues;

    %%%%%%%%%%%%%%%%%%%%%%%%%
    %simulate the models
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %the EGF concentration is 1000 in all experiments
    %note that there are also 3 control experiments, which are however never
    %simulated
    ICs(1) = 1000;

    %Vlad email: We were indeed using 1000 for EGF and 100 for Cilostamide,
    %EPACA and PKAA in an active state
    %Exp1: 27: EPACA, 28: Cilostamide, 29: PKAA
    ICs(27) = 0; ICs(28) = 0; ICs(29) = 0;
    sr1 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    % IN THE EXP DATA FILE EXP 2 AND 3 ARE SWAPPED
    %Exp2: 27: EPACA
    ICs(27) = 100; ICs(28) = 0; ICs(29) = 0;
    sr3 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    %Exp3: 28: Cilostamide
    ICs(27) = 0; ICs(28) = 100; ICs(29) = 0;
    sr2 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    %Exp4: 29: PKAA
    ICs(27) = 0; ICs(28) = 0; ICs(29) = 100;
    sr4 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    %Exp5: 27: EPACA, 28: Cilostamide
    ICs(27) = 100; ICs(28) = 100; ICs(29) = 0;
    sr5 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    % IN THE EXP DATA FILE EXP 6 AND 7 ARE SWAPPED
    %Exp6: 27: EPACA, 29: PKAA
    ICs(27) = 100; ICs(28) = 0; ICs(29) = 100;
    sr7 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    %Exp7: 28: Cilostamide, 29: PKAA
    ICs(27) = 0; ICs(28) = 100; ICs(29) = 100;
    sr6 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);
    %Exp8: 27: EPACA, 28: Cilostamide, 29: PKAA
    ICs(27) = 100; ICs(28) = 100; ICs(29) = 100;
    sr8 = IQMPsimulate('mexModel',2400,ICs,paramNames,paramValues,options);


    %get the predictions at the experimental time points
    %y = ERKPP/(ERK + EKRPP)
    simuResults = zeros(size(persistentData.expData.measurements));
    simuResults(1,:) = interp1(sr1.time,sr1.variablevalues(:,1),timePoints);
    simuResults(2,:) = interp1(sr2.time,sr2.variablevalues(:,1),timePoints);
    simuResults(3,:) = interp1(sr3.time,sr3.variablevalues(:,1),timePoints);
    simuResults(4,:) = interp1(sr4.time,sr4.variablevalues(:,1),timePoints);
    simuResults(5,:) = interp1(sr5.time,sr5.variablevalues(:,1),timePoints);
    simuResults(6,:) = interp1(sr6.time,sr6.variablevalues(:,1),timePoints);
    simuResults(7,:) = interp1(sr7.time,sr7.variablevalues(:,1),timePoints);
    simuResults(8,:) = interp1(sr8.time,sr8.variablevalues(:,1),timePoints);
    simuResults = 100*simuResults;

    %compute residuals
    residual = simuResults - persistentData.expData.measurements;
    residual = residual(:); % turn matrix into column vector

    %compute the cost function as the negative log likelihood
    l = length(persistentData.observableIdxs); % number of variables
    rho = l * length(timePoints);
    cost = 0.5*(rho*log(2*pi)+persistentData.expData.logDetS+transpose(residual)*persistentData.expData.invS*residual);

    % compute cost according to old function
    % stdsMat = zeros(size(simuResults));
    % for i = 1:size(simuResults,1)
    %     for j = 1:size(simuResults,2)
    %         pos = (j-1)*size(simuResults,1) + i;
    %         stdsMat(i, j) = sqrt(expData.S(pos,pos));
    %     end
    % end
    % wr = (simuResults - expData.measurements) ./ stdsMat;
    % sosq = sum(sum(wr.^2));
    % logT = log((2*pi)^(rho)*prod(prod(stdsMat))^2);
    % oldCost = 0.5 * (sosq + logT);

    if(nargout == 1)
        %return only cost
        varargout{1} = cost;
    elseif(nargout == 2)
        %return cost and simulation
        varargout{1} = cost;
        varargout{2} = simu;
    end
end

return



end

