function [ exit_status, results_fn, elapsed_time ] = = xuExperiment()
    error('Deprecated code'); % TODO adjust to the new code base
    assert(exist('TFmain') == 2, 'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

	% Beginning of program
	rng('shuffle');

	c = clock();
	fprintf('Program started at %d:%d\n',c(4),c(5));
	tic;

	% Specify path to source folder
	addpath(genpath('../../source'));

	p.modelFilename = './Xu4and2.txt';
    p.expFilename = './exp-EGF5tp7x.exp';
    p.expDataFilename = './expData.xls';
	p.paramSpecsFilename = './paramSpecs.txt';

    p.outputFilename = fullfile('../../results/xuTmp');

	% Specify parameters for exploration
    p.nmont = 3e3; %maximum number integrations in Metropolis Monte Carlo
    p.nelip = 6e3; %maximum number integrations in ellipsoids

	% Specify whether backtracking and/or recursive search should be performed
	p.backtrack = false;
	p.recursive = true;


    p.pointsAsOuter = true;
    p.parallelize = 1;

    p.localSearchThreshold = 0.5;
    p.globalSearchThreshold = 0.1;

    %% Run the actual program
    [ runs, viabilityThreshold, settings ] = TFmain(p);

    %% Save results
    results_fn = settings.outputFilename;
    save(results_fn, 'runs', 'viabilityThreshold', 'settings');

	% Cleanup
	c = clock();
	fprintf('Program finished at %d:%d\n',c(4),c(5));
    elapsed_time = toc;

    exit_status = 0;
end
