fname = './paramSpecs.txt';
paramNames = {'SosKcat', 'SosKm', 'reaction_20_Kcat', 'reaction_20_Km', 'reaction_21_Kcat', 'reaction_21_Km', 'reaction_24_Kcat', 'reaction_24_Km', 'reaction_25_Kcat', 'reaction_25_Km', 'reaction_26_V', 'reaction_26_Km', 'reaction_27_Kcat', 'reaction_27_Km', 'reaction_28_Kcat', 'reaction_28_Km', 'reaction_30_k1'};
bmin = 1e-5;
bmax = 1e5;
bprojection = zeros(length(paramNames),1);
for i = 1:length(paramNames)
	if strcmp(paramNames{i}(end-1:end),'Km')
		bprojection(i) = 1e-6;
	end
end

fid = fopen(fname, 'w');
for i = 1:length(paramNames)-1
	fprintf(fid, '%s %f %f %f\n', paramNames{i}, bprojection(i), bmin, bmax);
end
fprintf(fid, '%s %f %f %f', paramNames{end}, bprojection(end), bmin, bmax);
fclose(fid);
