Legacy data format
------------------

A space-separated values text file that includes:
  a. the time points of the measurements in the first line
  b. the names of the observed species in the second line
  c. the average measurements at each time point, for each species in a new line
  d. the measurement error as either
    1. two numbers: the relative error and relative base error (e.g. `0.1 0.02`, `modeString='relative'`)
    2. the standard deviation of each measurement, again as one line per species (`modeString='stds'`)
