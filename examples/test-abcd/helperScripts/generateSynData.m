%% Change working directory
% TODO: This is not very elegant... just a local, temporary solution for
% some extra convenience
if strcmp(cd, '/home/zenabi/topofilter')
    cd('./examples/test-abcd/');
end

%% Set parameters
numTimePoints = 300;
numObservables = 2;
maxT = 500;

timePoints = linspace(maxT/numTimePoints, maxT, numTimePoints);
observableIdxs = [1; 3];

%% load model and convert to mex Model
model = IQMmodel('model.txt');
[paramNames,paramValues] = IQMparameters(model);
[~,~,ICs] = IQMstates(model);

IQMmakeMEXmodel(model,'mexModel',0);

%% Simulate the model
errVal = 1e-12;             %value for relative and absolute tolerance
options.reltol = errVal;    %relative tolerance
options.abstol = errVal;    %absolute tolerance
options.maxnumsteps = 1e6;  %maximum number of steps
% set values for k5, k6, k7 to 0
paramValues(5:7) = 0;
simu = IQMPsimulate('mexModel', maxT, ICs, paramNames, paramValues, options);

%% Get the predictions at the experimental time points
simuResults = zeros(numObservables, numTimePoints);
perturbedResults = zeros(numObservables, numTimePoints);
variances = zeros(numObservables, numTimePoints);
for i = 1:length(observableIdxs)
    obs = observableIdxs(i);
    simuResults(i,:) = interp1(transpose(simu.time),simu.statevalues(:,obs),timePoints);
    stds = simuResults(i,:)*0.1 + max(simuResults(i,:))*0.02;
    stds = ones(1,numTimePoints)*0;
    tmp = zeros(10,numTimePoints);
    for j = 1:10
    	tmp(j,:) = simuResults(i,:) + randn(1,numTimePoints).*stds;
    end
    perturbedResults(i,:) = mean(tmp);
    variances(i,:) = var(tmp);
end

%% Write simulation results to file
fid = fopen('expDataGeneratedExact300.txt', 'w');
% Write time points
fprintf(fid, '%5.2f ', timePoints(1, 1:numTimePoints-1));
fprintf(fid, '%5.2f\n', timePoints(1, numTimePoints));
% Write observables
fprintf(fid, 'A C\n');
%Write simulation results
for obsId = 1:2
    fprintf(fid, '%8.6f ', perturbedResults(obsId, 1:numTimePoints-1));
    fprintf(fid, '%8.6f\n', perturbedResults(obsId, numTimePoints));
end
%Write standard deviations
for obsId = 1:2
    fprintf(fid, '%8.6f ', sqrt(variances(obsId, 1:numTimePoints-1)));
    fprintf(fid, '%8.6f\n', sqrt(variances(obsId, numTimePoints)));
end

fclose(fid);

%% Clean up and go back to top directory
%clear
cd('../../')
