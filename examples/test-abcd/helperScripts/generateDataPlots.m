function generateDataPlots
    close all;

    %% Simulate the model
    maxT = 500;
    model = IQMmodel('m2.txt');
    [paramNames,paramValues] = IQMparameters(model);
    [~,~,ICs] = IQMstates(model);

    errVal = 1e-12;             %value for relative and absolute tolerance
    options.reltol = errVal;    %relative tolerance
    options.abstol = errVal;    %absolute tolerance
    options.maxnumsteps = 1e6;  %maximum number of steps
    % set values for k5, k6, k7 to 0
    paramValues(5:7) = 0;
    simu = IQMPsimulate('mexModel', maxT, ICs, paramNames, paramValues, options);

    createFigure('expDataFromFigure', simu);
    createFigure('expDataFromFigureInterpolated', simu);
    createFigure('expDataGenerated', simu);
    createFigure('expDataGeneratedExact', simu);
    
    close all;
end

function createFigure(dataset, simu)
    %% Read and process the experimental data
    filename = ['./examples/toys/model2/' dataset '.txt'];
    fid = fopen(filename);

    % First line contains time points
    tmpLine = fgetl(fid);
    timePoints = cell2mat(textscan(tmpLine, '%f'));
    numTimePoints = length(timePoints);

    % Second line contains names of observable species
    tmpLine = fgetl(fid);
    tmpCell = textscan(tmpLine, '%s');
    observables = tmpCell{1};
    numObservables = length(observables);

    % The following numObservable lines each contain numTimePoints measurements
    measurements = zeros(numObservables, numTimePoints);
    for i = 1:numObservables
        tmpLine = fgetl(fid);
        measurements(i,:) = cell2mat(textscan(tmpLine, '%f'));
    end

    stds = zeros(numObservables, numTimePoints);
    for i = 1:numObservables
        tmpLine = fgetl(fid);
        stds(i,:) = cell2mat(textscan(tmpLine, '%f'));
    end

    %% Generate plot
    fig = figure;
    subplot(211);
    hold on;
    box on;
    plot(transpose(simu.time), simu.statevalues(:,1),'k');
    errorbar(timePoints,measurements(1,:),stds(1,:), 'k.');
    xlim([0 550]);
    ylim([50 150]);
    xlabel('t');
    ylabel('[A](t)');
    subplot(212);
    hold on;
    box on;
    plot(transpose(simu.time), simu.statevalues(:,3),'k');
    errorbar(timePoints,measurements(2,:),stds(2,:), 'k.');
    xlim([0 550]);
    ylim([20 60]);
    xlabel('t');
    ylabel('[C](t)');

    % set(gca, 'Position', get(gca, 'OuterPosition') - get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);

    % Here we preserve the size of the image when we save it.
    width = 2.3;     % Width in inches
    height = 2;    % Height in inches
    % set(gcf,'InvertHardcopy','on');
    set(gcf,'PaperUnits', 'inches');
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [width height]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 width height]);

    % Save the file as PDF
    set(gcf, 'renderer', 'painters');
    print(['~/Polybox/TopoFiltering/Report/figures/model2/' dataset],'-dpdf','-r300')
end