% Wrapper script that reads in results from previous
% experiments and visualizes the outcome.

%timepoints = 3;
%stds = [5 10 15];
timepoints = [3 5 8];
stds = 5;
counts = zeros(2^7, max(length(timepoints), length(stds)));
if length(timepoints) == 1
	for i = 1:length(stds)
		load(sprintf('../results/%d_%d.mat', timepoints, stds(i)));
		counts(:,i) = generateCountVector(runs);
	end
	generateModelCountPlots(counts, stds, 'rel. std.');
else
	for i = 1:length(timepoints)
		load(sprintf('../results/%d_%d.mat', timepoints(i), stds));
		counts(:,i) = generateCountVector(runs);
	end
	generateModelCountPlots(counts, timepoints, 'number time points');
end