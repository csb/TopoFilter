function h = generateModelCountPlots(counts, xtlabels, xlabelstring)
% GENERATEMODELCOUNTPLOTS Can be used to generate an array of
%   bar plots that visualize how often a model is found viable
%   under a set of conditions. (Similar to Fig. 6 in the report.)
    foundProjIdxs = find(sum(counts(:,:), 2));
    titleStr = dec2bin(foundProjIdxs,7);

    figure;

    nrows = 4;
    ncols = 6;

    nvals = size(counts, 2);
    for i = 1:length(foundProjIdxs)
        subplot(nrows, ncols, i);
        bar(1:nvals,counts(foundProjIdxs(i),:));
        title(titleStr(i,:));
        set(gca,'ylim',[0 11]);
        set(gca,'Xtick',1:nvals,'XTickLabel',xtlabels)
        if (mod(i-1,ncols) == 0)
           ylabel('counts');
        end
        if i > length(titleStr)-ncols
           xlabel(xlabelstring);
        end
    end

    % Here we preserve the size of the image when we save it.
    width = 8/5 * ncols;     % Width in inches
    height = 3/2 * nrows;    % Height in inches
    % set(gcf,'InvertHardcopy','on');
    set(gcf,'PaperUnits', 'inches');
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [width height]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 width height]);

end