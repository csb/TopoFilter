addpath(genpath('../../source'))

fname = fullfile('../../results/varyNmont');

%% Beginning of program
rng('shuffle');

c = clock();
fprintf('Program started at %d:%d\n',c(4),c(5));

modelFilename = './model.txt';
paramSpecsFilename = './paramSpecs.txt';

% Specify whether backtracking and/or recursive search should be performed
backtrack = true;
recursive = true;

% Specify number of runs
numRuns = 10;

% Get exp data
expData = readExpData(20, 0.1); % @MR ??? readExpData(filename, modeStr)

% Specify values for nmont
vals = [1e2 1e3 1e4];

counts = zeros(2^7, length(vals));
runtimes = zeros(1, length(vals));

for expIdx = 1:length(vals)

	tic;

	nmont = vals(expIdx);
    nelip = 2*nmont;

	startFile;

	for i = 1:size(viableProjectionsCollection,1)
	    decProjection = bin2dec(num2str(viableProjectionsCollection(i,:)));
	    counts(decProjection, expIdx) = counts(decProjection, expIdx)+1;
	end

	runtimes(expIdx) = toc;

	save(fname);
end

%% Cleanup
delete('./functionfinal.m', 'mexModel.mexa64');
c = clock();
fprintf('Program finished at %d:%d\n',c(4),c(5));
