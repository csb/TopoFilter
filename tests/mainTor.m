%% run
%
% Attention: torTest/testRun may take a while to finish.
%
% From command line:
%     matlab -nodisplay -nosplash -nodesktop -r "mainTor; exit"
%

clear('all'); % cf. possible changes in ExpData class


dfn = sprintf('torTest_%s.log',datestr(now, 'yymmdd_HHMM'));
fprintf('Writing to diary ''%s''..\n',dfn);
diary(dfn);

disp('torTest diary');
disp('=============');


testResults = runtests('torTest.m');

%% report

disp(testResults);

% >= R2014b
%trt = table(testResults); 
% < R2014b
trt = table(...
    {testResults.Name}', ...
    [testResults.Passed]', ...
    [testResults.Failed]', ...
    [testResults.Incomplete]', ...
    [testResults.Duration]', ...
    'VariableNames', {'Name', 'Passed', 'Failed', 'Incomplete', 'Duration'});
% writetable(trt,'testResults.xls')
disp(sortrows(trt,{'Failed' 'Incomplete'},'descend'));

diary('off');
disp('.. done');
