%% run

clear all; % cf. possible changes in ExpData class
testResults = runtests('toyTest.m');
disp(testResults);

%% analyse

% >= R2014b
%trt = table(testResults); 
% < R2014b
trt = table(...
    {testResults.Name}', ...
    [testResults.Passed]', ...
    [testResults.Failed]', ...
    [testResults.Incomplete]', ...
    [testResults.Duration]', ...
    'VariableNames', {'Name', 'Passed', 'Failed', 'Incomplete', 'Duration'});
% writetable(trt,'testResults.xls')
disp(sortrows(trt,{'Failed' 'Incomplete'},'descend'));
