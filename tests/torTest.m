function tests = torTest()
%TORTEST Basic scale test using tor example.
    tests = functiontests(localfunctions);

% For running only selected tests uncomment the code below, and adjust the
% second line:
%    tests = functiontests({@setupOnce, @teardownOnce, @setup, @teardown,...
%        @testModelTwo});
%        @testModelOneToFour});

end

%% Auxilaries

function [ testCase ] = runTf(testCase)
    % run TF experiment for the tests
    if ~isfield(testCase.TestData,'dryRun')
        testCase.TestData.dryRun = false;
    end
    if ~isfield(testCase.TestData,'numRuns')
        testCase.TestData.numRuns = 1;
    end
    if ~isfield(testCase.TestData,'paramSpecsFilename')
        testCase.TestData.paramSpecsFilename = 'paramSpecs_02.txt';
    end
    if ~isfield(testCase.TestData,'modelFilename')
        testCase.TestData.modelFilename = './model_02-observables.txt';
    end
    if ~isfield(testCase.TestData,'p0')
        testCase.TestData.p0 = 0;
    end
    if ~isfield(testCase.TestData,'enumLevel')
        testCase.TestData.enumLevel = 0;
    end
    if ~isfield(testCase.TestData,'parallelize')
        testCase.TestData.parallelize = 1;
    end
    if ~isfield(testCase.TestData,'saveViablePoints')
        testCase.TestData.saveViablePoints = false;
    end
    if ~isfield(testCase.TestData,'nfeval')
        testCase.TestData.nfeval = 200;
    end
    if ~isfield(testCase.TestData,'rfeval')
        testCase.TestData.rfeval = [4 5];
    end
    if ~isfield(testCase.TestData,'pvmin')
        testCase.TestData.pvmin = 0.1;
    end

    reportSetup(testCase);
    [exit_status, results_fn, elapsed_time] = torExperiment(...
        'dryRun',testCase.TestData.dryRun,...
        'numRuns',testCase.TestData.numRuns,...
        'paramSpecsFilename',testCase.TestData.paramSpecsFilename,...
        'modelFilename',testCase.TestData.modelFilename,...
        'p0',testCase.TestData.p0,...
        'enumLevel',testCase.TestData.enumLevel,...
        'parallelize',testCase.TestData.parallelize,...
        'saveViablePoints',testCase.TestData.saveViablePoints,...
        'nfeval',testCase.TestData.nfeval,...
        'rfeval',testCase.TestData.rfeval,...
        'pvmin',testCase.TestData.pvmin);

    testCase.TestData.exitStatus = exit_status;
    testCase.TestData.resultsFn = results_fn;
    testCase.TestData.elapsedTime = elapsed_time;

    verifyEqual(testCase,...
        exist(testCase.TestData.resultsFn,'file'), 2,...
        'Results file not found.');
    testCase.TestData.results = load(testCase.TestData.resultsFn);
    verifyEqual(testCase,...
        testCase.TestData.numRuns, numel(testCase.TestData.results.runs),...
        'Did not collect results for all runs.');
end

function reportSetup(testCase)
tn = testCase.TestData.testName;
fprintf('\n\n\n%s\n%s\n',tn,regexprep(tn, '.', '-'));

fprintf('\n\n### Setup\n');

printkv = @(k,v) fprintf('\t%s = %s\n',k,mat2str(v));
printkv('pwd',pwd);
printkv('numRuns',testCase.TestData.numRuns);
printkv('paramSpecsFilename',testCase.TestData.paramSpecsFilename);
printkv('p0',testCase.TestData.p0);
printkv('enumLevel',testCase.TestData.enumLevel);
printkv('parallelize',testCase.TestData.parallelize);
printkv('nfeval',testCase.TestData.nfeval);
printkv('rfeval',testCase.TestData.rfeval);
printkv('pvmin',testCase.TestData.pvmin);

fprintf('\n\n### Run\n');
end


%% Once setup and teardown
function setupOnce(testCase)
    import matlab.unittest.fixtures.CurrentFolderFixture
    testCase.TestData.expFolder = '../examples/kuepfer-tor';
    testCase.applyFixture(CurrentFolderFixture(testCase.TestData.expFolder));

    if (exist('cpuinfo','file') == 2)
        % report CPU info
        fprintf('\n\n');
        disp('CPU info');
        disp('--------');
        disp(cpuinfo);
    end

    %debuglevel(1); % DEBUG
end

function teardownOnce(testCase)
end


%% Per test setup and teardown
function setup(testCase)
end

function teardown(testCase)
%    if isfield(testCase.TestData,'resultsFn')
%        delete(testCase.TestData.resultsFn);
%    end
end


%% Tests

function testOutput(testCase)
%TESTOUTPUT Sanity check for torExperiment setup.
%
    testCase.TestData.testName = 'testOutput';
    testCase.TestData.numRuns = 1;
    testCase.TestData.dryRun = true;
    testCase.TestData.parallelize = 0;
    testCase = runTf(testCase);

    verifyEqual(testCase, testCase.TestData.exitStatus, 0,...
        'Exit status not 0.');
    verifyTrue(testCase, isfield(testCase.TestData.results,'viabilityThreshold') &&...
        isnumeric(testCase.TestData.results.viabilityThreshold));

    testCase.TestData.viabilityThreshold.expected = 43.08; % TODO via expData.viabilityThreshold;
    testCase.TestData.viabilityThreshold.absTol = 1e-2;
    verifyEqual(testCase, testCase.TestData.results.viabilityThreshold,...
        testCase.TestData.viabilityThreshold.expected,...
        'AbsTol', testCase.TestData.viabilityThreshold.absTol,...
        'Viability threshold is off - have you messed with the data?');

    verifyTrue(testCase, isfield(testCase.TestData.results,'runs') &&...
        numel(testCase.TestData.results.runs) == testCase.TestData.numRuns);
    for i=1:numel(testCase.TestData.numRuns)
        verifyTrue(testCase, isfield(testCase.TestData.results.runs{i},'error0') &&...
            isnumeric(testCase.TestData.results.runs{i}.error0));
        verifyTrue(testCase, ...
            testCase.TestData.results.runs{i}.error0 < testCase.TestData.results.viabilityThreshold,...
            'Initial point error under viability threshold.');
        verifyTrue(testCase, ...
            testCase.TestData.results.runs{i}.error0 < 36.7,... % TODO via expData.cost(nsols);
            'Initial point error got worse.');
    end

    if isfield(testCase.TestData,'resultsFn') && exist(testCase.TestData.resultsFn,'file')
        delete(testCase.TestData.resultsFn);
    end

end

function testModelTwo(testCase)
%TESTMODELTWO Sampling stress test with a large non-projectable core
%  (24 parameters), and small set of projectable extension parameters (5).
%
    testCase.TestData.testName = 'testModelTwo';
    testCase.TestData.numRuns = 1;
    testCase.TestData.parallelize = 1;
    %testCase.TestData.saveViablePoints = true;
    testCase.TestData.nfeval = 5e1;
    testCase.TestData.paramSpecsFilename = 'paramSpecs_02.txt';
    paramSpecs = loadParamSpecs( testCase.TestData );
    testCase = runTf(testCase);

    results = testCase.TestData.results;

    projectableMask = ~isnan(paramSpecs.projections);
    nonprojectableMask = isnan(paramSpecs.projections);
    nEssential = sum(nonprojectableMask);
    for i=1:numel(results.runs)
        verifyTrue(testCase,...
            numel(results.runs{i}.essentialParametersCollection) >= nEssential,...
            'Expecting at least 24 essential parameters: non-projectable ''k1--24''.');
        nProj = size(results.runs{i}.viableProjectionsCollection,1);
        verifyEqual(testCase,...
            size(results.runs{i}.paramSamplesCollection,1), nProj,...
            'Expecting a single viable parameter point for one viable projection.');
        sample = transpose(results.runs{i}.paramSamplesCollection(1,:));
        verifyTrue(testCase,...
            all(sample(projectableMask) ~= paramSpecs.projections(projectableMask)),...
            'Expecting all of the projectable parameters not to be projected in the sample for the empty projection (init point).');
        verifyTrue(testCase,...
            all((sample >= paramSpecs.bmin) & (sample <= paramSpecs.bmax)),...
            'Expecting all parameters values in the sample for the empty projection (init point) to be within defined bounds.');
        for j=2:nProj
            sample = transpose(results.runs{i}.paramSamplesCollection(j,:));
            verifyTrue(testCase,...
                any(sample(projectableMask) == paramSpecs.projections(projectableMask)),...
                'Expecting some of the projectable parameters to be projected in the sample viable parameter point.');
            verifyTrue(testCase,...
                all((sample(nonprojectableMask) >= paramSpecs.bmin(nonprojectableMask)) & (sample(nonprojectableMask) <= paramSpecs.bmax(nonprojectableMask))),...
                'Expecting nonprojectable parameters values in the sample to be within defined bounds.');
        end
    end
end


function testModelOneToFour(testCase)
%TESTMODELONETOFOUR Sampling and projections stress test with a large
%  non-projectable core (24 parameters) and multiple projectable extensions
%  (18 parameters).
%
    testCase.TestData.testName = 'testModelOneToFour';

    %testCase.TestData.dryRun = true;

    testCase.TestData.numRuns = 1;
    testCase.TestData.parallelize = 2;

    testCase.TestData.modelFilename = 'model_01_02_03_04-observables.txt';
    testCase.TestData.paramSpecsFilename = 'paramSpecs_01_02_03_04.txt';

    %testCase.TestData.saveViablePoints = true;
    %testCase.TestData.nfeval = 1e2;

    % load paramSpecs prior to setting testCase.TestData.p0 = 0 in `runTf`
    paramSpecs = loadParamSpecs( testCase.TestData );
    verifyEqual(testCase,...
        zeros(numel(paramSpecs.names),1), paramSpecs.p0,...
        'Non-zero initial params values - have you messed with the param specs?');
    model = loadModel( testCase.TestData );
    [~, paramSpecs.values] = getIQMparametersProps(model, paramSpecs.names);

    testCase = runTf(testCase);

    results = testCase.TestData.results;
    absTol = 1e-2;
    error0max = 39; % OPT compute it w/ evalModel
    for i=1:numel(results.runs)
         verifyEqual(testCase,...
            results.runs{i}.p0, paramSpecs.values, 'AbsTol', absTol,...
            'Initial params values are off.');
         verifyTrue(testCase, ...
            testCase.TestData.results.runs{i}.error0 < testCase.TestData.results.viabilityThreshold,...
            'Error for the initial parameter values is over the viability threshold for data.'); % should not happen; should be an error earlier
         verifyTrue(testCase,...
            results.runs{i}.error0 < error0max,...
            'Initial error is off - have you messed with the data or the model?');
    end
end
