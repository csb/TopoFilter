function tests = toyTest()
%TOYTEST Basic user-side test using the toy example.

    tests = functiontests(localfunctions);

% For running only selected tests uncomment this and adjust from the 2nd
% line:
%     tests = functiontests({@setupOnce, @teardownOnce, @setup, @teardown,...
%         @testEssentialPars4a,...
%         @testEssentialPars4b,...
%         @testEssentialPars4Coupled,...
%         @testEssentialPars4a_sample3...
%     });
end


%% Auxilaries

function [ testCase ] = runTf(testCase)
    % run TF experiment for the tests
    if ~isfield(testCase.TestData,'dryRun')
        testCase.TestData.dryRun = false;
    end
    if ~isfield(testCase.TestData,'numRuns')
        testCase.TestData.numRuns = 4;
    end
    if ~isfield(testCase.TestData,'paramSpecsFilename')
        testCase.TestData.paramSpecsFilename = 'paramSpecs_4a.txt';
    end
    if ~isfield(testCase.TestData,'couplingMatrixFcn')
        testCase.TestData.couplingMatrixFcn = @couplingMatrix;
    end
    if ~isfield(testCase.TestData,'p0')
        testCase.TestData.p0 = 0;
    end
    if ~isfield(testCase.TestData,'enumLevel')
        testCase.TestData.enumLevel = 2;
    end
    if ~isfield(testCase.TestData,'parallelize')
        testCase.TestData.parallelize = 1;
    end
    if ~isfield(testCase.TestData,'saveViablePoints')
        testCase.TestData.saveViablePoints = false;
    end
    if ~isfield(testCase.TestData,'nfeval')
        testCase.TestData.nfeval = 100;
    end
    if ~isfield(testCase.TestData,'rfeval')
        testCase.TestData.rfeval = [4 5];
    end
    if ~isfield(testCase.TestData,'pvmin')
        testCase.TestData.pvmin = 0.1;
    end

    [exit_status, results_fn, elapsed_time] = toyExperiment(...
        'dryRun',testCase.TestData.dryRun,...
        'numRuns',testCase.TestData.numRuns,...
        'paramSpecsFilename',testCase.TestData.paramSpecsFilename,...
        'couplingMatrixFcn',testCase.TestData.couplingMatrixFcn,...
        'p0',testCase.TestData.p0 ,...
        'enumLevel',testCase.TestData.enumLevel ,...
        'parallelize',testCase.TestData.parallelize,...
        'saveViablePoints',testCase.TestData.saveViablePoints,...
        'nfeval',testCase.TestData.nfeval,...
        'rfeval',testCase.TestData.rfeval,...
        'pvmin',testCase.TestData.pvmin);

    testCase.TestData.exitStatus = exit_status;
    testCase.TestData.resultsFn = results_fn;
    testCase.TestData.elapsedTime = elapsed_time;

    verifyEqual(testCase,...
        exist(testCase.TestData.resultsFn,'file'), 2,...
        'Results file not found.');
    testCase.TestData.results = load(testCase.TestData.resultsFn);
    verifyEqual(testCase,...
        testCase.TestData.numRuns, numel(testCase.TestData.results.runs),...
        'Did not collect results for all runs.');
end



function [testCase, vpProjected, vpSampled] = loadSingleViablePointsSets(testCase)
    [vpProjected, vpfnProjected] = loadSingleViablePointsSet(testCase, 'projected');
    [vpSampled, vpfnSampled] = loadSingleViablePointsSet(testCase, 'sampled');
    % for clean-up in teardown
    testCase.TestData.viablePointsFiles = {vpfnProjected; vpfnSampled};
end
function [ret, viablePointsFileName] = loadSingleViablePointsSet(testCase, typeName)
    [pathstr,name,~] = fileparts(testCase.TestData.resultsFn);
    viablePointsFile = ...
        dir(fullfile(pathstr, sprintf('%s_viablePoints_%s_*.mat', name, typeName)));

    assertEqual(testCase,...
        length(viablePointsFile), 1,...
        'Expecting one file with viable points.');
    viablePointsFileName = fullfile(pathstr, viablePointsFile.name);
    ret = load(viablePointsFileName);
end



%% Once setup and teardown
function setupOnce(testCase)
    import matlab.unittest.fixtures.CurrentFolderFixture
    testCase.TestData.expFolder = '../examples/toy-ligand-fluorescent';
    testCase.applyFixture(CurrentFolderFixture(testCase.TestData.expFolder));

    %debuglevel(1); % DEBUG
end

function teardownOnce(testCase)
end


%% Per test setup and teardown
function setup(testCase)
end

function teardown(testCase)
   if isfield(testCase.TestData,'resultsFn')
       delete(testCase.TestData.resultsFn);
   end
   if isfield(testCase.TestData,'viablePointsFile')
       delete(testCase.TestData.viablePointsFile);
   end
   if isfield(testCase.TestData,'viablePointsFiles') && iscellstr(testCase.TestData.viablePointsFiles)
       for i=1:numel(testCase.TestData.viablePointsFiles)
           delete(testCase.TestData.viablePointsFiles{i});
       end
   end
end


%% Tests

function testOutput(testCase)
    testCase.TestData.dryRun = true;
    testCase.TestData.numRuns = 1;
    testCase.TestData.parallelize = 0;
    testCase = runTf(testCase);

    verifyEqual(testCase, testCase.TestData.exitStatus, 0,...
        'Exit status not 0.');
    verifyTrue(testCase, isfield(testCase.TestData.results,'viabilityThreshold') &&...
        isnumeric(testCase.TestData.results.viabilityThreshold));
end

function testThreshold(testCase)
% cf. Simon report, Sec. 2, par. "Experimental setup"
    testCase.TestData.dryRun = true;
    testCase.TestData.numRuns = 1;
    testCase.TestData.parallelize = 0;
    testCase = runTf(testCase);

    y = [ 10 ];
    delta = [ 0.0707 ];
    sd = delta*y;
    logDetS = 2*sum(log(sd));
    % cf. Simon report, Eq. (2)
    testCase.TestData.viabilityThreshold.expected = calcThresholdSd(numel(y), logDetS);
    testCase.TestData.viabilityThreshold.absTol = 1e-6;
    %testCase.TestData.viabilityThreshold.expected = 2.4864;
    %testCase.TestData.viabilityThreshold.absTol = 1e-4;

    verifyEqual(testCase, testCase.TestData.results.viabilityThreshold,...
        testCase.TestData.viabilityThreshold.expected,...
        'AbsTol', testCase.TestData.viabilityThreshold.absTol,...
        'Viability threshold is off - have you changed the data or the threshold multiplier?');
    % + sanity checks for the threshold's SD multiplier
    lowerThreshold = calcThresholdSd(numel(y), logDetS, 1);
    verifyTrue(testCase, testCase.TestData.results.viabilityThreshold > lowerThreshold);
    higherThreshold = calcThresholdSd(numel(y), logDetS, 3);
    verifyTrue(testCase, testCase.TestData.results.viabilityThreshold < higherThreshold);
end

function testInitPointAndError(testCase)
    testCase.TestData.dryRun = true;
    % bonus: implicitly test parallel setup
    testCase.TestData.numRuns = 3;
    testCase.TestData.parallelize = 1;
    % bonus: implicitly test partial log- and back transformations in p0 (k3)
    testCase.TestData.paramSpecsFilename = 'paramSpecs_4a_k3.txt';
    % load paramSpecs prior to setting TestData.p0 = 0 in `runTf`
    paramSpecs = loadParamSpecs( testCase.TestData );
    testCase = runTf(testCase);

    results = testCase.TestData.results;
    absTol = 1e-2;
    error0 = 1.57; % OPT compute it w/ evalModel
    for i=1:numel(results.runs)
        verifyEqual(testCase,...
            results.runs{i}.p0, paramSpecs.p0, 'AbsTol', absTol,...
            'Initial parmas values are off - have you changed the param specs?');
        verifyTrue(testCase, ...
            testCase.TestData.results.runs{i}.error0 < testCase.TestData.results.viabilityThreshold,...
            'Initial point error under viability threshold.');
        verifyEqual(testCase,...
            results.runs{i}.error0, error0, 'AbsTol', absTol,...
            'Initial error is off - have you changed the data?');
    end
end

function testInitPointSearch(testCase)
    testCase.TestData.dryRun = true;
    testCase.TestData.numRuns = 1;
    testCase.TestData.parallelize = 0;
    testCase.TestData.paramSpecsFilename = 'paramSpecs_4a_nop0.txt';

    testCase = runTf(testCase);

    results = testCase.TestData.results;
    paramSpecs = loadParamSpecs( testCase.TestData.paramSpecsFilename );
    for i=1:numel(results.runs)
        p0 = results.runs{i}.p0;
        verifyTrue(testCase,...
            ~all(p0 == paramSpecs.p0),...
            'Initial params values are same as specified.');
        verifyTrue(testCase, ...
            testCase.TestData.results.runs{i}.error0 < testCase.TestData.results.viabilityThreshold,...
            'Initial point error under viability threshold.');
        verifyTrue(testCase,...
            all(p0 >= paramSpecs.bmin & p0 <= paramSpecs.bmax),...
            'Initial point is out of defined bounds.');
    end

end

function testEssentialPars4a(testCase)
% reproduce: Simon report, Fig. 4a)
    testCase.TestData.parallelize = 0;
    testCase.TestData.numRuns = 1;
    testCase.TestData.paramSpecsFilename = 'paramSpecs_4a.txt';
    testCase = runTf(testCase);

    results = testCase.TestData.results;
    paramSpecs = loadParamSpecs( testCase.TestData.paramSpecsFilename );
    absTol = 1e-6;
    for i=1:numel(results.runs)
        verifyEqual(testCase,...
            results.runs{i}.viableProjectionsCollection, [false false; true false],...
            'Expecting empty and ''k1'' viable projections.');
        verifyTrue(testCase,...
            strcmp(results.runs{i}.essentialParametersCollection,'k2'),...
            'Expecting only second parameter ''k2'' to be essential.');

        verifyEqual(testCase,...
            size(results.runs{i}.paramSamplesCollection), [2 2],...
            'Expecting a one viable parameter point for each viable projection.');
        sample = results.runs{i}.paramSamplesCollection(2,:);
        verifyEqual(testCase,...
            sample(1),paramSpecs.projections(1), 'AbsTol', absTol,...
            'Expecting parameter ''k1'' to be projected in the sample viable parameter point.');
        verifyTrue(testCase,...
            sample(2) >= paramSpecs.bmin(2) &&  sample(2) <= paramSpecs.bmax(2),...
            'Expecting parameter ''k2'' value in the sample to be within defined bounds.');
    end
end

function testEssentialPars4b(testCase)
% reproduce: Simon report, Fig. 4b)
    testCase.TestData.numRuns = 1;
    testCase.TestData.paramSpecsFilename = 'paramSpecs_4b.txt';
    testCase = runTf(testCase);

    results = testCase.TestData.results;
    paramSpecs = loadParamSpecs( testCase.TestData.paramSpecsFilename );
    absTol = 1e-6;
    for i=1:numel(results.runs)

        % Note: not really necessary here, as projections will come out
        %       sorted for a 2-D problem (due to punique(..., 'sorted')
        [viableProjectionsSorted, IviableProjectionsCollection] = sortrows(results.runs{i}.viableProjectionsCollection);
        IviableProjectionsSorted = zeros(1,size(viableProjectionsSorted,1));
        IviableProjectionsSorted(IviableProjectionsCollection) = 1:numel(IviableProjectionsSorted);
        %viableProjectionsSorted = results.runs{i}.viableProjectionsCollection;
        %IviableProjectionsSorted = 1:size(viableProjectionsSorted,1);

        verifyEqual(testCase,...
            viableProjectionsSorted, [false false; false true; true false; true true],...
            'Expecting all possible projections of parameters (''k1'', ''k2'') to be viable.');
        verifyTrue(testCase,...
            isempty(results.runs{i}.essentialParametersCollection),...
            'Expecting none of the parameters to be essential.');

        verifyEqual(testCase,...
            size(results.runs{i}.paramSamplesCollection), [4 2],...
            'Expecting four viable parameter points since all three projections are viable.');

        sample1 = results.runs{i}.paramSamplesCollection(IviableProjectionsSorted(2),:);
        verifyEqual(testCase,...
            sample1(2), paramSpecs.projections(2), 'AbsTol', absTol,...
            'Expecting parameter ''k2'' to be projected in the second sample viable parameter point.');
        verifyTrue(testCase,...
            sample1(1) >= paramSpecs.bmin(1) &&  sample1(1) <= paramSpecs.bmax(1),...
            'Expecting parameter ''k1'' value in the sample to be within defined bounds.');

        sample2 = results.runs{i}.paramSamplesCollection(IviableProjectionsSorted(3),:);
        verifyEqual(testCase,...
            sample2(1), paramSpecs.projections(1), 'AbsTol', absTol,...
            'Expecting parameter ''k1'' to be projected in the third sample viable parameter point.');
        verifyTrue(testCase,...
            sample2(2) >= paramSpecs.bmin(2) &&  sample2(2) <= paramSpecs.bmax(2),...
            'Expecting parameter ''k2'' value in the sample to be within defined bounds.');

        sample3 = results.runs{i}.paramSamplesCollection(IviableProjectionsSorted(4),:);
        verifyEqual(testCase,...
            sample3, paramSpecs.projections',...
            'AbsTol', absTol,...
            'Expecting both ''k1'' and ''k2'' to be projected in the fourth sample viable parameter point.');
    end
end

function testEssentialPars4Coupled(testCase)
% Couple k1=>k2 but not k2=>k1 and:
% 4a) fail due to k2 projection value <7.5 (cf. Simon report, Fig. 4a)
% 4b) succeed but without one param projections
%
    testCase.TestData.parallelize = 0;
    testCase.TestData.numRuns = 1;
    testCase.TestData.couplingMatrixFcn = 'toyCouplingMatrix';

    testCase.TestData.enumLevel = 1;
    % or 0, but with 2 the test should fail since OAT but not OAT-sum
    % projection of k2 only will be also in the results

    testCase.TestData.paramSpecsFilename = 'paramSpecs_4a.txt';
    testCase = runTf(testCase);
    results = testCase.TestData.results;
    for i=1:numel(results.runs)
        verifyEqual(testCase,...
            results.runs{i}.viableProjectionsCollection, [false false],...
            'Expecting only empty viable projections.');
        verifyTrue(testCase,...
            all(strcmp(results.runs{i}.essentialParametersCollection,'k1') | strcmp(results.runs{i}.essentialParametersCollection,'k2')),...
            'Expecting both parameters to be essential.');

        verifyEqual(testCase,...
            size(results.runs{i}.paramSamplesCollection), [1 2],...
            'Expecting a one viable parameter point for each viable projection.');
    end

    testCase.TestData.paramSpecsFilename = 'paramSpecs_4b.txt';
    testCase = runTf(testCase);
    results = testCase.TestData.results;
    paramSpecs = loadParamSpecs( testCase.TestData.paramSpecsFilename );
    absTol = 1e-6;
    for i=1:numel(results.runs)
        verifyEqual(testCase,...
            unique(results.runs{i}.viableProjectionsCollection,'rows','sorted'), [false false; true true],...
            'Expecting empty and full viable projections of parameters (''k1'', ''k2'') to be viable.');
        verifyTrue(testCase,...
            isempty(results.runs{i}.essentialParametersCollection),...
            'Expecting none of the parameters to be essential.');

        verifyEqual(testCase,...
            size(results.runs{i}.paramSamplesCollection), [2 2],...
            'Expecting two viable parameter points.');

        sample3 = results.runs{i}.paramSamplesCollection(2,:);
        verifyEqual(testCase,...
            sample3, paramSpecs.projections',...
            'AbsTol', absTol,...
            'Expecting both ''k1'' and ''k2'' to be projected in the fourth sample viable parameter point.');
    end
end


function testEssentialPars4a_sample3(testCase)
% Like Simon report Fig. 4a), but with k1 = 0 projection, and instead k3
% allowed to be sampled (in log space) to compensate.
%
% More specificaly, for k1=0, from Eq. (6), we have:
%     x2* = (k2/k4) + k3 * (x1(0)/k4)
% and when sampling only k1 and k2, the second term in the sum can't be
% adjusted to get sufficiently close to x2* value for k1 > 0
% (k2/k4; Eq. (5)), which was used to generate data. Now, for sufficiently
% small k3, the method should be able to classify k1=0 projection also as
% viable.
%

    testCase.TestData.parallelize = 0;
    testCase.TestData.numRuns = 1;
    testCase.TestData.paramSpecsFilename = 'paramSpecs_4a_k3.txt';
    testCase = runTf(testCase);

    results = testCase.TestData.results;
    paramSpecs = loadParamSpecs( testCase.TestData.paramSpecsFilename );
    absTol = 1e-6;
    for i=1:numel(results.runs)
        verifyEqual(testCase,...
            results.runs{i}.viableProjectionsCollection, [false false false; true false false],...
            'Expecting empty projection and projection of only first out of three parameters (''k1'', ''k2'', ''k3'') to be viable.');
        verifyEqual(testCase,...
            numel(results.runs{i}.essentialParametersCollection), 2,...
            'Expecting two essential parameters.');
        verifyTrue(testCase,...
            strcmp(results.runs{i}.essentialParametersCollection(1),'k2'),...
            'Expecting projectable ''k2'' to be essential.');
        verifyTrue(testCase,...
            strcmp(results.runs{i}.essentialParametersCollection(2),'k3'),...
            'Expecting non-projectable ''k3'' to be essential.');
        verifyEqual(testCase,...
            size(results.runs{i}.paramSamplesCollection), [2 3],...
            'Expecting a two viable parameter points for two viable projections.');
        sample = transpose(results.runs{i}.paramSamplesCollection(2,:));
        verifyEqual(testCase,...
            sample(1),paramSpecs.projections(1), 'AbsTol', absTol,...
            'Expecting parameter ''k1'' to be projected in the sample viable parameter point.');
        verifyTrue(testCase,...
            all((sample(2:3) >= paramSpecs.bmin(2:3)) & (sample(2:3) <= paramSpecs.bmax(2:3))),...
            'Expecting parameters ''k2'' and ''k3'' values in the sample to be within defined bounds.');
        verifyTrue(testCase,...
            sample(3) < 0.1*paramSpecs.p0(3),...
            'Expecting parameter ''k3'' value to have much smaller value than the initial value.');
    end
end


function NOtestParameterSamplingQuasiunif(testCase)
% WARN: this test is valid only if using HYPERSPACE/homosamp at the end of
%       the (re)sampling workflow in the TopoFilter. That's not currently
%       the case. The sample obtained in the ellipsoid-based sampling of
%       HYPERSPACE 1.2 is more dense at the boundaries than in the
%       interior of the viable space (which makes more sense for the
%       projections out of the sampling bounds).
%
% For simplicity: check marginals in a search box encapsulating only
% a rectangular viable space.
%
    testCase.TestData.numRuns = 1;
    testCase.TestData.paramSpecsFilename = 'paramSpecs_rectangle.txt';

    testCase.TestData.nfeval = 1e2;
    testCase.TestData.rfeval = [1 100];
    testCase.TestData.pvmin = 0.01; % practically: sample, but no-resampling
    testCase.TestData.saveViablePoints = true;
    testCase.TestData.dropMontPoints = true;

% % When the viable space gets more complicated and "turns" towards (0,0),
% % the test should fail
%     testCase.TestData.paramSpecsFilename = 'paramSpecs_4a_extended.txt';

    testCase = runTf(testCase);

    paramSpecs = loadParamSpecs( testCase.TestData.paramSpecsFilename );
    [testCase, vpProjected, vpSampled] = loadSingleViablePointsSets(testCase);

    vpProjected = vpProjected.viablePoints;
    verifyTrue(testCase,...
        all(~vpProjected.projection),...
        'Expecting projected sample for an empty projection.');
    verifyEqual(testCase,...
        size(vpProjected.rowmat,1),1,...
        'Expecting a single projected sample.');
    p0s = vpProjected.rowmat(1,:);
    p0s(vpProjected.islog)=10.^p0s(vpProjected.islog);
    verifyEqual(testCase,...
        p0s, transpose(paramSpecs.p0), 'AbsTol', 1e-6,...
        'Expecting a first projected sample to be the initial point.');

    vpSampled = vpSampled.viablePoints;
    threshold = testCase.TestData.results.viabilityThreshold;
    verifyTrue(testCase,...
        all(vpSampled.cost <= threshold),...
        'Expecting all viable samples cost to be lower than viability threshold.');

    vp = vpSampled.rowmat;
    dim = size(vp,2);
    if dim == 2,
        figure();
        scatterhist(vp(:,1),vp(:,2));
        title('testParameterSamplingQuasiunif, marginal dists');
        xlabel(vpSampled.colnames{1}); ylabel(vpSampled.colnames{2});
    end

    for i = 1:numel(vpSampled.colnames)
        pName = vpSampled.colnames{i};
        iMod = find(strcmp(paramSpecs.names,pName));
        verifyTrue(testCase,...
            ~isempty(iMod),...
            sprintf('Expecting ''%s'' to be sampled.',pName));
        verifyEqual(testCase,...
            vpSampled.islog(i), paramSpecs.islog(iMod), ...
            sprintf('Log-space specification inconsistency for ''%s''.',pName));

        vpI = vpSampled.rowmat(:,i);
        a = paramSpecs.bmin(iMod);
        b = paramSpecs.bmax(iMod);
        if (vpSampled.islog(i))
            a=log10(a);
            b=log10(b);
        end
        verifyTrue(testCase,...
            all((vpI  >= a) & (vpI  <= b)),...
            sprintf('Expecting all samples for ''%s'' to be within defined bounds.',pName));
        % Check sample uniformity: fine if we cannot reject viablePoints
        % marginal distribution with one-sample Kolomogorov-Smirnov test at
        % 99% confidence level.
        if dim > 2,
            figure(); hist(vpI);
            title(sprintf('testParameterSamplingQuasiunif, ''%s'' marginal dist',pName));
        end
        [h, p] = kstest(vpI,'CDF',[vpI unifcdf(vpI, a, b)],'Alpha',0.001);
        %fprintf('Is %s uniform in [%.3g %.3g]? K-S test p-value = %.3g.\n',pName, a, b, p);
        verifyTrue(testCase, ~h,...
            sprintf('Expecting quasi-uniform marginal sample for ''%s''; p-value for k-test was: %.3g.',pName, p));
    end
end
