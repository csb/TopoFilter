Intro
=====

This MATLAB package implements and extends the topological filtering method initially described in [[6]].

The code corresponding directly to the topological filtering method is in the `topologicalFiltering.m` and `getViableProjections.m` source files.


Installation
============

Download and install first the IQM Tools toolbox v1.2.1 [[3]], for handling models and experimental data. It requires a correctly setup MEX-compiler (`mex --setup` in MATLAB Command Window).

Additionally, if you want to take advantage of the parallelisation support, you need to setup the Parallel Computing Toolbox [[4]].

Now download HYPERSPACE toolbox v1.2.1 [[2]], for the parameter space exploration, and the TopoFilter package v0.3.2 [[1]]. Make sure that only source folders with sub-folders from both HYPERSPACE and TopoFilter tollboxes are on your MATLAB path (w/o examples and tests folders).


Quickstart
==========

First take a look into the `examples/toy-ligand-fluorescent` folder to see a very basic example.

To analyze your model against data, you need to create the following files:

1. A description of the experimental data sets in possibly multiple CSV or XLS files, following again IQM Tools specifications [[5]] for the `IQMmeasurement` data format.

    Remark: Saving XLS files with LibreOffice is known to sometimes cause problems. Saving with Microsoft Excel should fix it.

    Out: `expData.{txt,xls}` files.

2. A description of your model following the IQM Tools specifications [[5]] for one of the `IQMmodel` model formats.

    Out: `model.{txt,xml}` file.

3. Description of how to run the model to reproduce conditions for each of the previously prepared data sets; again follow the IQM Tools specifications [[5]], this time for the `IQMexperiment` format.

    Out: `expSetup.exp` files.

4. A description of the model parameters in a CSV text file with a fixed header:

        names,projections,bmin,bmax,islog[,p0]

    in which subsequent comma-separated entries in every line denote, respectively:

        <parameter name>,
        <parameter projection value or empty if sampled but not projected>,
        <parameter value lower bound for exploration>,
        <parameter value upper bound for exploration>,
        <sample in log space flag given as 0 or 1>,
        <(optional column) initial parameter value>

    e.g.

        names,projections,bmin,bmax,islog,p0
        k1,0,0.001,100,1,0.1
        k2,1e-4,1e-2,1e+1,0,1e+1
        k3,,1e-2,1e+2,1,1e+0

    Optionally, you can also specify coupled projections of parameters by defining own `myCouplingMatrix` function definition (see `help couplingMatrix` for details).

    Out: `paramSpecs.txt` file, and optionally `myCouplingMatrix.m` file.

5. Prepare experiment function. Copy `templateExperiment.m` into the folder containing your files and adjust it accordingly. Start analysis by calling `TFmain` function.

    Remark: to print debugging information use `setDebugLvl` function (higher level gives more output).

    Out: `myExperiment.m` MATLAB script or function file.


**Make sure that `source` folder with sub-folders is on your MATLAB path.** Now you can run the `experiment.m` file you've prepared.

After execution is finished, the results for each of the experiment repeats will be saved in the file specified in `outputFilename`. This file contains a MATLAB cell array named `runs`, with cells containing resulting variables for each of the runs you've computed. The most important variable is the matrix `viableProjectionsCollection` which contains in its rows all viable projections that were found as boolean vectors (`1` if the parameter was reduced), and `paramSamplesCollection` matrix with parameter values witness sample for each of the corresponding projections.


Options
=======

Parameters for the execution are specified in the experiment function (e.g. `myExperiment.m` above). They should be added as fields of an options `struct` passed to the package's main function: `TFmain`. For testing different setups, you can use a MATLAB `inputParser` object to make your experiment function take optional name-value parameters by  (cf. `toyExperiment.m`). The following options are available:

|Parameter name|Type|Default|Description|
|--------------|----|-------|-----------|
|`paramSpecsFilename`|`string`|*required* (*)|name of file that specifies parameter space exploration|
|`modelFilename`|`string`|*required* (*)|name of file that specifies model (SBML (TXT/XML) or IQMmodel (TXT/Matlab) format)|
|`expFilename`|`string` or `string cell array`|*required* (*)|name of file that specifies experiments (IQMexperiment format; one experiment expected per each data file)|
|`expDataFilename`|`string` or `string cell array`|*required* (*)|name of file that specifies data (IQMmeasurement format (CSV or XLS); in case of XLS each sheet (except for first) is treated like a single CSV)|
|`expDataIdxs`|`posint vector`|`[]`|indices for data files to use (helpful w/ multi-sheet XLS); `[]` = all|
|`couplingMatrixFcn`|`string or fun handle`|`@couplingMatrix`|coupled projections specification; see `help couplingMatrix` for details|
|`outputFilename`|`string`|`'tempname().mat'`|name of results file|
|`numRuns`|`posint`|`1`|number of runs|
|`dryRun`|`logical`|`false`|flag to calculate only threshold (and p0, if not given)|
|`nmont`|`posint`|`89`|maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling|
|`nelip`|`posint`|`111`|guiding number of model evaluations in the Ellipsoids-based sampling|
|`nvmin`|`posint`|`20`|minimum number of viable parameter values required not to (re-)sample; `1` = never sample, `2` = sample and re-sample only if 1 viable point is left for some projection (i.e. practically never)|
|`nvmax`|`posint`|`380`|maximum number of viable parameter values (before sparsifying it); note: parameter values set may grow big due to merging of points for projections obtained in more than one way)|
|`saveViablePoints`|`logical`|`false`|flag for saving viable points after each (maximal) projection and each sampling; files will be saved next to `outputFilename` with its base name as a prefix|
|`p0`|`numeric vector` or `0`|`0`|initial parameter point, if `0` taken from the paramaters specification, or from the model (in that order)|
|`calcCostFcn`|`string or fun handle`|`@calcCost`|method of computing of the cost of fit of observables to the corresponding data points; see e.g. `help calcCost`|
|`calcCostFcnArgs`|`cell array`|`{}`|extra args for the `calcCostFcn`|
|`calcThresholdFcn`|`string or fun handle`|`@calcThresholdQuantile`|method of computing of the cost  threshold; see e.g. `help calcThresholdQuantile` or `help calcThresholdSd`|
|`calcThresholdFcnArgs`|`cell array`|`{}`|extra args for the `calcThresholdFcn`|
|`odeIntegratorOptions`|`struct`|`struct()`|Structure with ODE integrator options as specified in `IQMPsimulate`|
|`odeIntegratorMaxnumstepsTry`|`posint`|`3`|number of integration attempts in case of an error; each next with 10-times previous `maxnumsteps` (default: `1e5`)|
|`odeIntegratorSolNrTimepoints`|`int`|`0`|number of points in the integration output, i.e. `tspan = 0:(tmax/ntp):tmax` (default: `0`, means use `IQMPsimulate` default); note: `t0` is always assumed to be `0`|
|`parallelize`|`int`, `0 .. 3`|`0`|level of parallelization<br/>`0` - none<br/>`1` - (recommended) over OAT viability checks and, if required, over found maximal projections during viable points preparation<br/>`2` - over recursive searches, and in the pre-recursion search in the same way as for lower levels<br/>`3` - over runs|
|`recursive`|`logical`|`true`|flag for recursive search; note: more efficient than running again `TFmain` externally because of the duplicates found while searching recursively|
|`enumLevel`|`int`, `0 .. 2`|`1`|enumeration level for the viable projections that allows to switch the heuristic between goals of: a) finding quickly a maximal reduction (`0`), and b) finding as many reductions as possible (`2`). A tradeoff between speed and exhaustiveness.|
|`dropMontPoints`|`logical`|`false`|flag indicating if viable points found with MCMC method should be dropped<br>(These are distributed along the MC random path, starting at initial sampling point, whereas the ellipsoids-based sampling ones are usually distributed over shells close to boundaries of viable subspace of the parameter space.)|
|`exhaustiveRank`|`posint`|`1`|number of (couples of) parameters to combine at most at once during the initial exhaustive search for the viable projections (`1` = singletons, `2` = singletons and pairs etc.); beware: number of OAT viability checks grows exponentially with this number|
|*EXPERIMENTAL*|||*do not use unless you know what you are doing*|
|`backtrack`|`logical`|`false`|flag for (exponential) backtracking of nested model from non-viable summed projections|
|`pointsAtOuter`|`logical`|`true`|flag for running over parameter points in the outer loop (as opposed to running over reduced dimensions)|


(*): It is also possible to specify parameters specification, model, experiments and experimental data directly (i.e. create objects yourself) using the `paramSpecs`, `model`, `experiments` and `expData` options, respectively. In that case `*Filename` options are not required.



Vision
======

The main goal currently is to target scalability. To that end, re-visit and fine-tune key functions of TopoFilter [[1]] and HYPERSPACE toolboxes [[2]], and start working on improvements of the method and code. Apply your ideas to successively larger models. EGFR and yTOR models are great playgrounds for testing ideas with, respectively, 17 (remaining parameters are kept at optimal values from original paper) and 40+ parameters to explore.

Consider also alternative methods for parameter exploration, in particular the adaptive Smolyak sparse grids method [[7]].


Authors
=======

Initial:

    Mikael Sunnåker <mikael.sunnaker@bsse.ethz.ch>

Current:

    Simon Möller <moellesi@ethz.ch>
    Mikołaj Rybiński <mikolaj.rybinski@bsse.ethz.ch>


References
==========

[1]: https://gitlab.com/csb.ethz/TopoFilter/tags
1. [D-BSSE GitLab: TopoFilter toolbox releases][1]
[2]: https://gitlab.com/csb.ethz/HYPERSPACE/tags
2. [D-BSSE GitLab: HYPERSPACE toolbox releases][2]
[3]: http://www.intiquan.com/iqm-tools/iqm-tools-download/
3. [IQM Tools Download page][3]
[4]: https://uk.mathworks.com/products/parallel-computing/
4. [MathWorks: Parallel Computing Toolbox product page][4]
[5]: [http://www.intiquan.com/iqm-tools/tutorial-iqm-tools/]
5. [IQM Tools Tutorials page][5]
[6]: http://www.ncbi.nlm.nih.gov/pubmed/23716718
6. [PMID: 23716718][6]
[7]: https://www.ncbi.nlm.nih.gov/pubmed/26317784
7. [PMID: 26317784][7]
